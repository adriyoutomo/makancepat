package adriyo.makancepat.ui.login;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.reactivestreams.Subscriber;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.UserModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.PelangganInterface;
import okhttp3.Response;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by adriyo on 3/23/2017.
 */

public class LoginPresenterTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    LoginView view;
    @Mock
    LoginInteractor loginInteractor;
    @Mock
    UserModel userModel;
    @Mock
    PelangganInterface apiService;

    private LoginPresenter loginPresenter;
    private ArgumentCaptor<Subscriber<Response>> subscriberArgumentCaptor;

    @Before
    public void setUp() throws Exception {
        apiService = Communicator.getClient().create(PelangganInterface.class);
        SessionManager sessionManager = null;
        loginPresenter = new LoginPresenter(view, loginInteractor, apiService, sessionManager);
    }

    @Test
    public void checkIfEmailIsEmptyShowError() throws Exception {
        when(view.getEmail()).thenReturn("");
//        loginPresenter.onLoginButtonClick();
        verify(view).showErrorEmptyEmail(R.string.error_invalid_email);
    }

    @Test
    public void checkIfPasswordIsEmptyShowError() throws Exception {
        when(view.getEmail()).thenReturn("asdasd");
        when(view.getPassword()).thenReturn("");
//        loginPresenter.onLoginButtonClick();
        verify(view).showErrorEmptyPassword(R.string.error_invalid_password);
    }

    @Test
    public void whenFormIsNotEmptyWithWrongCredential() throws Exception {
        when(view.getEmail()).thenReturn("admin");
        when(view.getPassword()).thenReturn("22");
        when(loginInteractor.validateLogin("admin", "22")).thenReturn(false);
//        loginPresenter.onLoginButtonClick();
        verify(view).showToastFailedLogin();
    }

    @Test
    public void whenFormIsNotEmptyWithRightCredential() throws Exception {
        when(view.getEmail()).thenReturn("admin");
        when(view.getPassword()).thenReturn("asu");
        when(loginInteractor.validateLogin("admin", "asu")).thenReturn(true);
//        loginPresenter.onLoginButtonClick();
        verify(view).startMainActivity();
    }

}