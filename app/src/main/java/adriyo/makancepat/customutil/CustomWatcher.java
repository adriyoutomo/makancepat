package adriyo.makancepat.customutil;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

/**
 * Created by ADMIN on 2/4/2017.
 */

public class CustomWatcher implements TextWatcher {

    private View view;

    public CustomWatcher(View view) {
        this.view = view;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
