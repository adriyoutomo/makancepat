package adriyo.makancepat.customutil;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by ADMIN on 2/23/2017.
 * referensi: https://pratamawijaya.com/programming/android-programming/retrofit-2-0-x-rxjava-upgrade-tutorial
 */


public class ErrorUtils {
    private static final String ERROR_NO_SERVER_CONNECTION = "No Server Connection";
    private static final String ERROR_BAD_REQUEST = "Erorr Bad Request";
    private static final String ERROR_UNEXPECTED = "error Unexpected";
    private static final String ERROR_NO_NETWORK_CONNECTION = "No Connection Error";

    public static String getErrorMessage(Throwable throwable) {
        if (throwable instanceof IOException) return ERROR_NO_NETWORK_CONNECTION;

        if (throwable instanceof HttpException) {
            if (((HttpException) throwable).code() == 403) return ERROR_NO_SERVER_CONNECTION;
            try {
                return new JSONObject(
                        ((HttpException) throwable).response().errorBody().source().readUtf8()).getString(
                        "userMessage");
            } catch (IOException | JSONException e) {
                return ERROR_BAD_REQUEST;
            }
        }
        return ERROR_UNEXPECTED;
    }
}