package adriyo.makancepat.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.RecyclerTouchListener;
import adriyo.makancepat.model.Model_TopUp;
import adriyo.makancepat.model.Model_TopUp_Response;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.Interface_TopUp;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static adriyo.makancepat.view.TopUpFragment.PICK_IMAGE;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class PesanFragment_Management extends Fragment {

    private int mColumnCount = 1;
    private final static String API_KEY = "b395769bf1111a7e5ec283a3b8bf66dc";
    private OnListFragmentInteractionListener mListener;
    private static final String TAG = HomeActivity.class.getSimpleName();
    RecyclerView recyclerView;
    private FragmentActivity myContext;
    List<Model_TopUp> topUp;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PesanFragment_Management() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pesan_list, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final TextView textView = (TextView) view.findViewById(R.id.errFragmentPesan);
        recyclerView = (RecyclerView) view.findViewById(R.id.listHeader);
        ((ManagementActivity) getActivity()).setMyTitleBar("PESAN");

        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(myContext));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(myContext, mColumnCount));
        }

        final Interface_TopUp apiService = Communicator.getClient().create(Interface_TopUp.class);
        Call<Model_TopUp_Response> modelTopUpCall = apiService.getNewTopUp();
        modelTopUpCall.enqueue(new Callback<Model_TopUp_Response>() {
            @Override
            public void onResponse(Call<Model_TopUp_Response> call, Response<Model_TopUp_Response> response) {
                topUp = response.body().getListTopUp();
                recyclerView.setAdapter(new PesanRecyclerViewAdapter_Management(topUp, mListener));

                if (topUp.size() > 0) {
                    textView.setText("");
                }
            }

            @Override
            public void onFailure(Call<Model_TopUp_Response> call, Throwable t) {
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int posisi) {
                final Dialog dialog = new Dialog(getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_management_message);

                TextView tittle_management_message = (TextView) dialog.findViewById(R.id.title_managemen_message);
                ImageView image_management_message = (ImageView) dialog.findViewById(R.id.image_management_message);
                Button button_management_message = (Button) dialog.findViewById(R.id.button_management_message);
                tittle_management_message.setText("Username = " + topUp.get(posisi).getUsername());
                //image_management_message.setImageBitmap(getBitmapFromURL(Communicator.getClient().baseUrl().toString() + "v1/images/" + topUp.get(idTransaksi).getImages() + ".png"));
                imagePath = topUp.get(posisi).getImages();
                Picasso.with(getContext()).load(Communicator.getClient().baseUrl().toString() + "v1/images/" + topUp.get(posisi).getImages() + ".png").into(mTarget);
                Picasso.with(getContext()).load(Communicator.getClient().baseUrl().toString() + "v1/images/" + topUp.get(posisi).getImages() + ".png").into(image_management_message);
                dialog.show();

                image_management_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        File a = new File(imagePath);
                        if (a.exists()) {
                            refreshGallery(a, getActivity());
                            Intent intent = new Intent();
                            intent.setDataAndType(Uri.fromFile(a), "image/*");
                            intent.setAction(Intent.ACTION_VIEW);
                            startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE);
                        }
                    }
                });

                button_management_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Call<Model_TopUp> modelTopUpCall = apiService.updateTopUp(topUp.get(posisi).getId());
                        modelTopUpCall.enqueue(new Callback<Model_TopUp>() {
                            @Override
                            public void onResponse(Call<Model_TopUp> call, Response<Model_TopUp> response) {
                                dialog.dismiss();
                                topUp.remove(posisi);
                                recyclerView.setAdapter(new PesanRecyclerViewAdapter_Management(topUp, mListener));
                                Toast.makeText(getContext(), "Sukses", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onFailure(Call<Model_TopUp> call, Throwable t) {
                                dialog.dismiss();
                                Toast.makeText(getContext(), "Gagal", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
            }
        }));
    }

    @Override
    public void onAttach(Context context) {
        myContext = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Model_TopUp item);
    }

    private Target mTarget = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            saveImage(bitmap);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            // Handle image load error
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    };

    private static String imagePath;

    private void saveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
        File myDir = new File(root);
        String fname = imagePath + ".png";
        File file = new File(myDir, fname);
        //if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            imagePath = file.getAbsolutePath();
            Log.d("GONO", "sukses");
            if (file.exists()) {
                Log.d("GONO", "ada");
                Log.d("GONO", Uri.fromFile(file).toString());
            } else {
                Log.d("GONO", "gak ada");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void refreshGallery(File file, Activity activity) {
        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(activity,
                new String[]{file.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });
    }
}
