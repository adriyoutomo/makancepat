package adriyo.makancepat.view;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.ItemDrawerAdapter;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.MenuMakananModel;
import adriyo.makancepat.model.RestoModel;
import adriyo.makancepat.ui.transaksi.CheckoutActivity;
import de.hdodenhof.circleimageview.CircleImageView;

public class ManagementRestoActivity extends AppCompatActivity
        implements RestoFragment.OnListFragmentInteractionListener,
        MenuMakananFragment.OnFragmentInteractionListener {
    boolean doubleBackToExitPressedOnce = false;

    View drawerView;
    DrawerLayout drawer;
    ItemDrawerAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private int lastExpandedPosition = -1;
    private int lastMenuClickedPosition = -1;
    NavigationView navigationView;
    TextView txtTitleBar;
    CircleImageView profileImage;
    boolean menuClickedOrNot = false;
    SessionManager sessionManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_home);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        txtTitleBar = (TextView) findViewById(R.id.txtTitleAppBar);
        setMyTitleBar("Makan Cepat");
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        expListView = (ExpandableListView) findViewById(R.id.drawerlist);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        prepareListData();
        selectItem(0);

        listAdapter = new ItemDrawerAdapter(this, listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
        expListView.setItemChecked(0, true);
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPosition, long l) {

                TextView txt = (TextView) view.findViewById(R.id.labelListDrawer);
                if (groupPosition != 11) {
                    int index = expandableListView.getFlatListPosition(ExpandableListView.getPackedPositionForGroup(groupPosition));
                    expandableListView.setItemChecked(index, true);
                    selectItem(groupPosition);
                    menuClickedOrNot = true;
                } else {
                    //txt.setTextColor(getResources().getColor(R.color.list_text_color));
                    menuClickedOrNot = false;
                }

                lastMenuClickedPosition = groupPosition;
                return menuClickedOrNot;
            }
        });
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
                    expListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View view, int groupPosition, int childPosition, long l) {
                int index = parent.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
                parent.setItemChecked(index, true);
                return false;
            }
        });
        View headerview = navigationView.getHeaderView(0);
        profileImage = (CircleImageView) headerview.findViewById(R.id.profile_image);
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ManagementRestoActivity.this, DetailProfilActivity.class);
                startActivity(intent);
            }
        });

        sessionManager = new SessionManager(getApplicationContext());
        TextView txtUsername = (TextView) headerview.findViewById(R.id.nav_header_txt_username);
        txtUsername.setText(sessionManager.getUserDetails().get(SessionManager.KEY_USERNAME));
    }

    /**
     * Swaps fragments in the main content view
     */
    private void selectItem(int position) {
        // Create a new fragment and specify the planet to show based on position
        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new HalamanDepanFragment();
                break;
            case 1:
                fragment = new RestoFragment();
                break;
            default:
                fragment = new HalamanDepanFragment();
                break;
        }
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();

        if(position == 5) showLogoutDialog();

        drawer.closeDrawer(GravityCompat.START);

    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding Header data
        listDataHeader.add("Halaman Depan");
        listDataHeader.add("Pesanan");

        listDataChild.put(listDataHeader.get(0), new ArrayList<String>());
        listDataChild.put(listDataHeader.get(1), new ArrayList<String>());

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //Checking for fragment count on backstack
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else if (!doubleBackToExitPressedOnce) {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit.", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                super.onBackPressed();
                //return;
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.home, menu);
        MenuInflater inflater = getMenuInflater();
        MenuItem searchItem = menu.findItem(R.id.menuHomeSearch);
        inflater.inflate(R.menu.home, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.menuHomeCart) {
            Intent intent = new Intent(getApplicationContext(), CheckoutActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.menuHomeSearch) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setMyTitleBar(String title) {
        txtTitleBar.setText(title);
    }

    public void showLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        // Add the buttons
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
                sessionManager.logoutUser();
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        builder.setMessage("Yakin ingin keluar?").setTitle("Konfirmasi");
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onListFragmentInteraction(RestoModel item) {

    }

    @Override
    public void onFragmentInteraction(MenuMakananModel item) {

    }
}
