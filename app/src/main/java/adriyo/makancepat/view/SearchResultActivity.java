package adriyo.makancepat.view;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.SearchRVAdapter;
import adriyo.makancepat.customutil.RecyclerTouchListener;
import adriyo.makancepat.model.MenuMakananModel;
import adriyo.makancepat.model.SearchResultResponse;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.SearchService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResultActivity extends AppCompatActivity {

    private List<MenuMakananModel> models;
    private RecyclerView recyclerView;
    private SearchRVAdapter adapter;
    LinearLayout layoutCheckout;
    String title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = getResources().getString(R.string.title_search);
        if (getSupportActionBar() == null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(title);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //NavUtils.navigateUpFromSameTask(CheckoutActivity.this);
                    finish();
                }
            });
        }

        recyclerView = (RecyclerView) findViewById(R.id.rvListPencarian);
        showError(true);
        handleIntent(getIntent());
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int posisi) {
                MenuMakananModel model = models.get(posisi);
                Intent intent = new Intent(SearchResultActivity.this, DetailMakananActivity.class);
                intent.putExtra("idMakanan", model.getIdMenu());
                startActivity(intent);
                finish();
            }
        }));

    }

    @Override
    protected void onNewIntent(Intent intent) {
        //setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            getSupportActionBar().setTitle(Html.fromHtml(title+ " <i>"+query+"</i>"));
            //use the query to search your data somehow
            loadData(query);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*MenuInflater inflater = getMenuInflater();
        MenuItem searchItem = menu.findItem(R.id.menuHomeSearch);
        inflater.inflate(R.menu.home, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }*/

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.menuHomeSearch).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        menu.getItem(1).setVisible(false);
        return true;
    }

    public void loadData(String query) {
        models = new ArrayList<>();
        SearchService apiService = Communicator.getClient().create(SearchService.class);
        apiService.searchData(query).enqueue(new Callback<SearchResultResponse>() {
            @Override
            public void onResponse(Call<SearchResultResponse> call, Response<SearchResultResponse> response) {
                models = response.body().getList();
                recyclerView.setAdapter(new SearchRVAdapter(models));
                showError(false);

                Log.d("size", models.size() + "");
                if (models.size() > 0) showError(false);
                else showError(true);
            }

            @Override
            public void onFailure(Call<SearchResultResponse> call, Throwable t) {
                Toast.makeText(SearchResultActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void showError(boolean show) {
        if (show) recyclerView.setVisibility(View.GONE);
        else recyclerView.setVisibility(View.VISIBLE);
        Log.d("showerror", "is called");
    }
}

