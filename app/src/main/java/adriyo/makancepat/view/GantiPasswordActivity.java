package adriyo.makancepat.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.SocketTimeoutException;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.UserModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.PelangganInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GantiPasswordActivity extends AppCompatActivity implements TextWatcher {

    private TextView txtCurrentPass, txtNewPass;
    private Button btnPass;
    private MyUtilities myUtilities;
    private SessionManager sessionManager;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ganti_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (getSupportActionBar() == null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Ganti Password");
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }
            });
        }

        init();
    }

    private void init() {
        txtCurrentPass = (TextView) findViewById(R.id.txtCurrentPassword);
        txtCurrentPass.addTextChangedListener(this);
        txtNewPass = (TextView) findViewById(R.id.txtNewPassword);
        txtNewPass.addTextChangedListener(this);
        btnPass = (Button) findViewById(R.id.btnSavePassword);
        checkFieldsForEmptyValues();
        myUtilities = new MyUtilities();
        sessionManager = new SessionManager(this);
        progressDialog = new ProgressDialog(GantiPasswordActivity.this);
        progressDialog.setMessage("Please Wait....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    public void savePassword(View view) {
//        checkPassword(sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL));
        updatePassword(
                txtCurrentPass.getText().toString().trim(),
                txtNewPass.getText().toString().trim(),
                sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL)
        );
    }

    private void checkFieldsForEmptyValues() {
        Button s = (Button) findViewById(R.id.btnSavePassword);
        String n = txtCurrentPass.getText().toString();
        String p = txtNewPass.getText().toString();
        if (n.length() > 6 && p.length() > 6) {
            s.setEnabled(true);
        } else {
            s.setEnabled(false);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        checkFieldsForEmptyValues();
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void checkPassword(String email) {
        showDialog(true);
        PelangganInterface service = Communicator.getClient().create(PelangganInterface.class);
        service.getUser(email).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.isSuccessful()) {
                    showDialog(false);
                    UserModel model = response.body();
                    String currPass = txtCurrentPass.getText().toString().trim();
                    if (!model.isError() && model.getPassword().equals(currPass)) {
                        String newPass = txtNewPass.getText().toString().trim();
//                        updatePassword(newPass, sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL));
                    } else {
                        txtCurrentPass.setError("Isi password anda dengan benar");
                    }
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                showDialog(false);
            }
        });
    }

    public void showDialog(boolean show) {
        if (show) progressDialog.show();
        else progressDialog.dismiss();
    }

    private void updatePassword(String oldPass, String newPassword, String email) {
        showDialog(true);
        PelangganInterface service = Communicator.getClient().create(PelangganInterface.class);
        service.updatePassword(oldPass, newPassword, email).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                showDialog(false);
                if (response.isSuccessful()) {
                    if (!response.body().isError()) {
                        Intent intent = new Intent();
                        intent.putExtra("message", "Ganti Password berhasil");
                        setResult(RESULT_OK, intent);
                        finish();
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                    }else{
                        Intent intent = new Intent();
                        intent.putExtra("message", response.body().getMessage());
                        setResult(RESULT_OK, intent);
                        finish();
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                    }
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                showDialog(false);
                if (t instanceof IOException) {
                    //Add your code for displaying no network connection error
                    Toast.makeText(GantiPasswordActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (t instanceof SocketTimeoutException) {
                    //Add your code for displaying no network connection error
                    Toast.makeText(GantiPasswordActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
