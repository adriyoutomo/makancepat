package adriyo.makancepat.view;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.ViewPagerAdapter;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.SaldoModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.PelangganInterface;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class DompetkuFragment extends Fragment {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public TextView txtSaldo;
    private SessionManager sessionManager;
    private static final String TAG = DompetkuFragment.class.getSimpleName();

    public DompetkuFragment() {
        // Required empty public constructor
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String idUser = sessionManager.getUserDetails().get(SessionManager.KEY_ID_USER);
        requestDataSaldo(idUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_dompetku, container, false);
        sessionManager = new SessionManager(getActivity());
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpagerDompetku);
        setupViewPager(viewPager);

        ((HomeActivity) getActivity()).setMyTitleBar("Dompetku");
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs_dompetku);
        tabLayout.setupWithViewPager(viewPager);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        String formattedDate = df.format(c.getTime());
        TextView textClock = (TextView) rootView.findViewById(R.id.textClock1);
        textClock.setText(formattedDate + " hari ini");

        txtSaldo = (TextView) rootView.findViewById(R.id.dompetkuTxtSaldo);
        return rootView;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new HistoryFragment(), "HISTORY");
        adapter.addFragment(new TopUpFragment(), "TOP UP");
        adapter.addFragment(new PencairanFragment(), "PENCAIRAN");
        viewPager.setAdapter(adapter);
    }


    public void requestDataSaldo(String idUser) {
        PelangganInterface service = Communicator.getClient().create(PelangganInterface.class);
        Observable<SaldoModel> saldoModel = service.getSaldo(Integer.parseInt(idUser));
        saldoModel.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(saldoModel1 -> {
                            txtSaldo.setText(MyUtilities.setRupiah(saldoModel1.getSaldo() + ""));
                        }, throwable -> {
//                            Toast.makeText(MyApplication.getContext(),
//                                    ErrorUtils.getErrorMessage(throwable)
//                                    , Toast.LENGTH_SHORT).show();
                        }
                );

    }

}
