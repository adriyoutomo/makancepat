package adriyo.makancepat.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import adriyo.makancepat.R;

public class CheckoutCompletedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_completed);
    }

    public void closeActivity(View view) {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        finish();
    }
}
