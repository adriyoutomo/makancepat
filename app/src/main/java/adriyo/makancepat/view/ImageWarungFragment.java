package adriyo.makancepat.view;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.ImageWarungAdapter;
import adriyo.makancepat.model.GaleriWarungResponse;
import adriyo.makancepat.model.ImageWarungModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.GaleriWarungInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageWarungFragment extends Fragment {


    private ViewPager mViewPager;
    private ImageWarungAdapter adapterView;
    private int idMenu;
    private List<ImageWarungModel> data;
    public ImageWarungFragment() {
        // Required empty public constructor
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            idMenu = bundle.getInt("idMenu", 0);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_image_warung, container, false);
        requestData(idMenu);
        mViewPager = (ViewPager) rootView.findViewById(R.id.viewPagerMenuMakanan);
        adapterView = new ImageWarungAdapter(getActivity(), data);
        mViewPager.setAdapter(adapterView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTab();
    }

    private void setTab() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int position) {
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageSelected(int position) {
            }

        });
    }

    public void requestData(int idMenu) {
        GaleriWarungInterface apiService = Communicator.getClient().create(GaleriWarungInterface.class);
        Call<GaleriWarungResponse> call = apiService.getGaleriWarung(idMenu);
        call.enqueue(new Callback<GaleriWarungResponse>() {
            @Override
            public void onResponse(Call<GaleriWarungResponse> call, Response<GaleriWarungResponse> response) {
                if (response.isSuccessful())
                    data = response.body().getList();
            }

            @Override
            public void onFailure(Call<GaleriWarungResponse> call, Throwable t) {
                // Log error here since request failed
                Toast.makeText(getActivity(), "Failed to request data", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
