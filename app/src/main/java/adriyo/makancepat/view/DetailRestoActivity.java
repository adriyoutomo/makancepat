package adriyo.makancepat.view;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.ImageMakananAdapter;
import adriyo.makancepat.adapter.ImageWarungAdapter;
import adriyo.makancepat.adapter.PagerRestoAdapter;
import adriyo.makancepat.adapter.RestoRecyclerViewAdapter;
import adriyo.makancepat.customutil.MyStatic;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.model.GaleriMakananResponse;
import adriyo.makancepat.model.GaleriWarungResponse;
import adriyo.makancepat.model.ImageMakananModel;
import adriyo.makancepat.model.ImageWarungModel;
import adriyo.makancepat.model.RestoModel;
import adriyo.makancepat.model.RestoResponse;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.GaleriMakananInterface;
import adriyo.makancepat.rest.GaleriWarungInterface;
import adriyo.makancepat.rest.RestoInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetailRestoActivity extends AppCompatActivity {

    ViewPager pager, slider;
    RestoModel model;
    List<RestoModel> listResto;
    TabLayout tabLayout;
    String id;
    TextView txtNamaResto, txtAlamatResto,txtHari, txtJam, txtDeskripsi;
    private ImageWarungAdapter adapterView;
    private List<ImageWarungModel> dataImage;
    ImageView logoResto;
    MyUtilities myUtilities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_resto);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (getSupportActionBar() == null) {
            Log.d("actionbar", "gak kosong");
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Detail Resto");
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //NavUtils.navigateUpFromSameTask(DetailMakananActivity.this);
                    finish();
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

                }
            });
        }
        Bundle extras = getIntent().getExtras();
        id = extras.getString("idResto");
        pager= (ViewPager) findViewById(R.id.pager);
        slider = (ViewPager) findViewById(R.id.slider);
        tabLayout= (TabLayout) findViewById(R.id.tab_layout);
        requestDataImage(Integer.parseInt(id));
        setupViewPager(pager);
        tabLayout.setupWithViewPager(pager);
        initUiComponent();


        RestoInterface apiService = Communicator.getClient().create(RestoInterface.class);
        Call<RestoModel> call = apiService.getDetailRestoById(Integer.parseInt(id));
        call.enqueue(new Callback<RestoModel>() {
            @Override
            public void onResponse(Call<RestoModel> call, Response<RestoModel> response) {
                if (response.isSuccessful()) {
                    RestoModel model = response.body();
                    //txtNamaResto.setText();
                    if(!model.isError()){
                        txtNamaResto.setText(model.getNamaWarung());
                        txtAlamatResto.setText(model.getAlamat());
                        txtHari.setText(model.getHari());
                        txtJam.setText(model.getJam());
                        txtDeskripsi.setText(model.getDeskripsi());
                    }else{
                        //Toast.makeText(getApplicationContext(), "Failed to Load Data", Toast.LENGTH_SHORT).show();
                        myUtilities.showInfoDialog(model.getMessage(), R.style.MyAlertDialogStyle, DetailRestoActivity.this, true);
                    }
                }
            }

            @Override
            public void onFailure(Call<RestoModel> call, Throwable t) {
                // Log error here since request failed
                //Toast.makeText(getActivity(), "Failed to request data", Toast.LENGTH_SHORT).show();
            }
        });

    }

    void initUiComponent(){
        logoResto = (ImageView) findViewById(R.id.logoResto);
        txtNamaResto = (TextView) findViewById(R.id.txtJudul);
        txtAlamatResto = (TextView) findViewById(R.id.txtAlamat);
        txtHari = (TextView) findViewById(R.id.txtHari);
        txtJam = (TextView) findViewById(R.id.txtJam);
        txtDeskripsi = (TextView) findViewById(R.id.txtDeskripsi);
        myUtilities = new MyUtilities();
    }

    public void requestDataImage(int idWarung) {
        GaleriWarungInterface apiService = Communicator.getClient().create(GaleriWarungInterface.class);
        Call<GaleriWarungResponse> call = apiService.getGaleriWarung(idWarung);
        call.enqueue(new Callback<GaleriWarungResponse>() {
            @Override
            public void onResponse(Call<GaleriWarungResponse> call, Response<GaleriWarungResponse> response) {
                if (response.isSuccessful()) {
                    dataImage = response.body().getList();
                    //Log.d("sizeDataImage",""+dataImage.size());
                    //Toast.makeText(getApplicationContext(), dataImage.get(0).getUrl(), Toast.LENGTH_SHORT).show();
                    if(dataImage.size() > 0) {
                        adapterView = new ImageWarungAdapter(DetailRestoActivity.this, dataImage);
                        slider.setAdapter(adapterView);
                    }else {
                        ImageWarungModel item = new ImageWarungModel();
                        item.setUrl(MyStatic.urlImageNotAvailable);
                        dataImage.add(item);
                        adapterView = new ImageWarungAdapter(DetailRestoActivity.this, dataImage);
                        slider.setAdapter(adapterView);
                    }

                }
            }

            @Override
            public void onFailure(Call<GaleriWarungResponse> call, Throwable t) {
                // Log error here since request failed
                Toast.makeText(DetailRestoActivity.this, "Failed to request data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        FragmentManager manager = this.getSupportFragmentManager();
        PagerRestoAdapter adapter = new PagerRestoAdapter(manager);
        //adapter.addFragment(new MakananRestoFragment(), "Makanan");
        //adapter.addFragment(new MinumanRestoFragment(), "Minuman");
        adapter.addFragment(new MakananRestoFragment(), "Makanan", id);
        adapter.addFragment(new MinumanRestoFragment(), "Minuman", id);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

    }
}
