package adriyo.makancepat.view;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.PesananUserRVAdapter;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.customutil.RecyclerTouchListener;
import adriyo.makancepat.model.DaftarPesananUserModel;
import adriyo.makancepat.model.DaftarPesananUserResponse;
import adriyo.makancepat.model.DetailPesananUserModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.DaftarPesananUserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PembelianFragment extends Fragment {

    private int mColumnCount = 1;
    private static final String TAG = HomeActivity.class.getSimpleName();
    RecyclerView recyclerView;
    private FragmentActivity myContext;
    private List<DaftarPesananUserModel> data = new ArrayList<>();
    private List<DetailPesananUserModel> detailData = new ArrayList<>();
    private SessionManager sessionManager;
    private int idUser;
    private MyUtilities myUtilities;
    private ProgressBar progressBar;

    public PembelianFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pembelian, container, false);
        progressBar = (ProgressBar) view.findViewById(R.id.pbPembelian);
        recyclerView = (RecyclerView) view.findViewById(R.id.rvDaftarPesananUser);
        sessionManager = new SessionManager(myContext);
        idUser = Integer.parseInt(sessionManager.getUserDetails().get(SessionManager.KEY_ID_USER));
        myUtilities = new MyUtilities(getActivity());
        ((HomeActivity) getActivity()).setMyTitleBar("Transaksi");
        myUtilities.showLoading(progressBar, true);
        requestData(idUser);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int posisi) {
                detailData = data.get(posisi).getDetailPesanan();
                StringBuffer strBuf = new StringBuffer();
                String getDetail = "";
                for (DetailPesananUserModel item : detailData) {
                    String row = "\n" + item.getNamaMenu() + "\n Rp." + item.getHarga() + " x " + item.getQty() + " : Rp. " + (item.getHarga() * item.getQty());
                    strBuf.append(row);
                }
                Bundle args = new Bundle();
                args.putInt("idTransaksi", data.get(posisi).getIdTransaksi());
                Intent intent = new Intent(getActivity(), DetailPemesananUserActivity.class);
                intent.putExtras(args);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        }));

    }

    @Override
    public void onAttach(Context context) {
        myContext = (FragmentActivity) context;
        super.onAttach(context);
    }

    private void requestData(int idUser) {
        DaftarPesananUserInterface apiService = Communicator.getClient().create(DaftarPesananUserInterface.class);

        Call<DaftarPesananUserResponse> call = apiService.getDaftarPesanan(idUser);
        call.enqueue(new Callback<DaftarPesananUserResponse>() {
            @Override
            public void onResponse(Call<DaftarPesananUserResponse> call, Response<DaftarPesananUserResponse> response) {
                myUtilities.showLoading(progressBar, false);
                if (response.isSuccessful()) {

                    data = response.body().getListMenu();
                    //Toast.makeText(myContext, "data:"+data.size(), Toast.LENGTH_SHORT).show();
                    recyclerView.setAdapter(new PesananUserRVAdapter(data));
                } else {
                    Log.i("errBeliFragment", response.message());
                }
            }

            @Override
            public void onFailure(Call<DaftarPesananUserResponse> call, Throwable t) {
                // Log error here since request failed
                //Toast.makeText(getActivity(), "Failed to request data", Toast.LENGTH_SHORT).show();
                Log.e("errorCok", t.getMessage());
                myUtilities.showLoading(progressBar, true);
            }
        });
    }

}
