package adriyo.makancepat.view;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.DetailUserLVAdapter;
import adriyo.makancepat.customutil.MyPermissionChecker;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.customutil.PermissionsActivity;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.UserModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.PelangganInterface;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetailProfilActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    private static final String[] PERMISSIONS_READ_STORAGE = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};

    private UserModel model;

    @BindView(R.id.lvDetailUser)
    ListView listView;

    private DetailUserLVAdapter adapter;

    @BindView(R.id.txtProfilNama)
    TextView txtNama;
    @BindView(R.id.txtProfilEmail)
    TextView txtEmail;
    @BindView(R.id.txtProfilUsername)
    TextView txtUsername;

    @BindView(R.id.profile_image)
    CircleImageView imageView;
    private CoordinatorLayout coordinatorLayout;
    private Button snackbar;
    private int RESULT_EDIT_PROFILE = 1001;
    private int RESULT_EDIT_PASSWORD = 1002;
    private int GALLERY_INTENT = 1003;
    private int GALLERY_INTENT_UP_KITKAT = 1004;
    private SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.errorLayout)
    LinearLayout errorLayout;

    @BindView(R.id.profileLayout)
    LinearLayout profilLayout;

    MyPermissionChecker checker;

    MyUtilities x;
    private SessionManager sessionManager;
    private String email = "";
    String imagePath;

    ProgressDialog progressDialog;
    String messageDialog = "";

    String username = "";

    boolean ifItemShouldBeEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_profil);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (getSupportActionBar() == null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("User Profile");
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavUtils.navigateUpFromSameTask(DetailProfilActivity.this);
//                    overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);

                }
            });
        }

        init();
        email = sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL);
        requestData(email);
        /*Retrofit retrofit = Communicator.getClient();
        PelangganInterface service = retrofit.create(PelangganInterface.class);
        service.getUser(email).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                Log.d("grescok",response.body().getLastName());

            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {

            }
        });

        PelangganInterface req = Communicator.getClient().create(PelangganInterface.class);
        req.checkLogin("adriyoutomo@gmail.com","lahedan").enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                Log.d("login_coba",response.body().getLastName()
                +response.body().getFoto());

            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {

            }
        });*/
    }

    private void init() {
        x = new MyUtilities();

        sessionManager = new SessionManager(getApplicationContext());
        sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL);

        imageView.setOnClickListener(this);

        checker = new MyPermissionChecker(this);

        progressDialog = new ProgressDialog(DetailProfilActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshDetailProfil);
        swipeRefreshLayout.setOnRefreshListener(this);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!ifItemShouldBeEnabled) {
        }

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case R.id.menuEditProfile:
                if (!ifItemShouldBeEnabled) {
                    Intent i = new Intent(this, EditProfileActivity.class);
                    startActivityForResult(i, RESULT_EDIT_PROFILE);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }
                return true;
            case R.id.menuEditPassword:
                if (!ifItemShouldBeEnabled) {
                    Intent i2 = new Intent(this, GantiPasswordActivity.class);
                    startActivityForResult(i2, RESULT_EDIT_PASSWORD);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detail_profil, menu);
        return true;
    }

    private void requestData(String email) {
        messageDialog = "Processing...";
        progressDialog.setMessage("Processing...");
//        progressDialog.show();
        PelangganInterface service = Communicator.getClient().create(PelangganInterface.class);
        service.getUser(email).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {

                    Log.d("fotobarxxu", response.body().getFoto());
                    model = response.body();

                    ArrayList<String> list = new ArrayList<>();
                    list.add(model.getAlamat());
                    list.add(model.getNoKtp());
                    list.add(model.getNoRekening());
                    adapter = new DetailUserLVAdapter(DetailProfilActivity.this, list);
                    listView.setAdapter(adapter);
                    txtNama.setText(model.getFirstName() + " " + model.getLastName());
                    txtEmail.setText(model.getEmail());
                    txtUsername.setText("@" + model.getUsername());
                    sessionManager.changeFoto(model.getFoto());
                    Picasso.with(DetailProfilActivity.this)
                            .load(Communicator.BASE_URL + model.getFoto().toLowerCase())
                            .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                            .placeholder(R.drawable.default_user_icon)
                            .into(imageView);
                    adapter.notifyDataSetChanged();
                    showErrorLayout(false);

                } else {
                    showErrorLayout(true);
                }

            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                progressDialog.dismiss();
                showErrorLayout(true);
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (null == data) return;

        if (requestCode == RESULT_EDIT_PROFILE && resultCode == Activity.RESULT_OK) {
            x.showInfoDialog(data.getStringExtra("message"), R.style.MyAlertDialogStyle, this, false);
            requestData(email);
        }
        if (requestCode == RESULT_EDIT_PASSWORD && resultCode == Activity.RESULT_OK) {
            x.showInfoDialog(data.getStringExtra("message"), R.style.MyAlertDialogStyle, this, false);
        }
        Uri originalUri = null;
        if (requestCode == GALLERY_INTENT) {
            originalUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(originalUri, filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = cursor.getString(columnIndex);
                cursor.close();
                if (!TextUtils.isEmpty(imagePath)) {
//                    Toast.makeText(this, "GALLERY_INTENT", Toast.LENGTH_SHORT).show();
                    uploadImage();
                }
            } else {
                x.showInfoDialog("Error...:(", R.style.MyAlertDialogStyle, this, false);
            }
        } else if (requestCode == GALLERY_INTENT_UP_KITKAT
                && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(originalUri, filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = cursor.getString(columnIndex);
                cursor.close();
                if (!TextUtils.isEmpty(imagePath)) {
//                    uploadImage();
                    Toast.makeText(this, "GALLERY_INTENT_KITKAT", Toast.LENGTH_SHORT).show();
                }
            } else {
                x.showInfoDialog("Error...:(", R.style.MyAlertDialogStyle, this, false);
            }
        }

    }

    public void showPhotoSpinner() {
        AlertDialog.Builder builder = new AlertDialog.Builder(DetailProfilActivity.this);
        builder.setItems(R.array.option_change_photo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int position) {
                if (checker.lacksPermissions(PERMISSIONS_READ_STORAGE)) {
                    startPermissionsActivity(PERMISSIONS_READ_STORAGE);
                } else {
                    // File System.
                    final Intent galleryIntent = new Intent();
                    galleryIntent.setType("image/*");
                    galleryIntent.setAction(Intent.ACTION_PICK);

                    // Chooser of file system options.
                    final Intent chooserIntent = Intent.createChooser(galleryIntent, "Choose image..");
                    startActivityForResult(chooserIntent, GALLERY_INTENT);
                }
//                openGallery();
            }
        });
        AlertDialog b = builder.create();
        b.setCancelable(true);
        b.setCanceledOnTouchOutside(true);
        b.show();
    }

    @Override
    public void onClick(View view) {
        if (view == imageView) {
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
            } else {
                // Pre-Marshmallow
            }
            showPhotoSpinner();
        }
    }

    private void openGallery() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent();
            intent.setType("image/jpeg");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Open Gallery"), GALLERY_INTENT);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/jpeg");
            startActivityForResult(intent, GALLERY_INTENT_UP_KITKAT);
        }
    }

    private void uploadImage() {
        messageDialog = "Uploading image...";
        progressDialog.setMessage("Uploading image...");
        progressDialog.show();

        PelangganInterface api = Communicator.getClient().create(PelangganInterface.class);
        File file = new File(imagePath);
        String extension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
        String filename = sessionManager.getUserDetails().get(SessionManager.KEY_USERNAME);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        RequestBody requestIdUser = RequestBody.create(MediaType.parse("text/plain"),
                sessionManager.getUserDetails().get(SessionManager.KEY_ID_USER));
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("uploaded_file", filename + extension, requestFile);
        RequestBody requestFoto = RequestBody.create(MediaType.parse("text/plain"),
                sessionManager.getUserDetails().get(SessionManager.KEY_FOTO).trim());
        Call<UserModel> call = api.uploadProfilePicture(body, requestIdUser, requestFoto);
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (!response.body().isError()) {
                        requestData(email);
                    } else {
                        x.showInfoDialog("Failed :(", R.style.MyAlertDialogStyle, DetailProfilActivity.this, false);
                    }
                } else {
                    x.showInfoDialog("No response from server...", R.style.MyAlertDialogStyle, DetailProfilActivity.this, false);
                }
                imagePath = "";
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                progressDialog.dismiss();
                x.showInfoDialog("No Response from server", R.style.MyAlertDialogStyle, DetailProfilActivity.this, false);
                imagePath = "";

            }
        });
    }

    private void startPermissionsActivity(String[] permission) {
        PermissionsActivity.startActivityForResult(this, 0, permission);
    }

    @Override
    public void onRefresh() {
        requestData(email);
    }

    public void showErrorLayout(boolean show) {
        if (show) {
            ifItemShouldBeEnabled = true;
            profilLayout.setVisibility(View.GONE);
            errorLayout.setVisibility(View.VISIBLE);
        } else {
            ifItemShouldBeEnabled = false;
            profilLayout.setVisibility(View.VISIBLE);
            errorLayout.setVisibility(View.GONE);
        }
    }
}
