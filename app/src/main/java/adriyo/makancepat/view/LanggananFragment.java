package adriyo.makancepat.view;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.ViewPagerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class LanggananFragment extends Fragment {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    public LanggananFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_langganan, container, false);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpagerLangganan);
        setupViewPager(viewPager);

        ((HomeActivity) getActivity()).setMyTitleBar("Langganan");
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs_langganan);
        tabLayout.setupWithViewPager(viewPager);
        return rootView;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new RestoLanggananFragment(), "RESTO LANGGANAN");
        adapter.addFragment(new PelangganFragment(), "PELANGGAN");
        viewPager.setAdapter(adapter);
    }
}
