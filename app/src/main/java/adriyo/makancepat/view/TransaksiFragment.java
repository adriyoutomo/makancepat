package adriyo.makancepat.view;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.ViewPagerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransaksiFragment extends Fragment {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    public TransaksiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_transaksi, container, false);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpagerTransaksi);

        ((HomeActivity) getActivity()).setMyTitleBar("Transaksi");
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs_transaksi);
        tabLayout.setupWithViewPager(viewPager);
        setupViewPager(viewPager);

        return rootView;
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new PembelianFragment(), "PEMBELIAN");
        //adapter.addFragment(new PenjualanFragment(), "PENJUALAN");
        viewPager.setAdapter(adapter);
    }

}
