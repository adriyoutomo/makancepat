package adriyo.makancepat.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.MyStatic;

/**
 * Created by ADMIN on 8/3/2016.
 */

public class FragmentLogin2 extends Fragment {

    public FragmentLogin2() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_screen_2, container, false);
        ImageView image = (ImageView) view.findViewById(R.id.imgBanner2);
        Picasso.with(getActivity()).load(MyStatic.MAIN_BANNER_2).placeholder(R.color.bg_color_dark).into(image);
        return view;
    }
}
