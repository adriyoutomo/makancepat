package adriyo.makancepat.view;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import adriyo.makancepat.BaseActivity;
import adriyo.makancepat.R;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.UserModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.PelangganInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityDaftar extends BaseActivity implements View.OnClickListener {

    private Button btnDaftar;
    private Toolbar toolbar;

    private EditText txtNamaDepan, txtNamaBelakang, txtUsername, txtEmail, txtPassword,
            txtNoTelp, txtAlamat, txtNoRekening, txtNoKTP, txtReferal, txtBank;
    //    private Spinner spinnerBank;
    private SessionManager sessionManager;

    private RadioButton btnRadioLaki, btnRadioPerempuan;

    ProgressDialog progressDialog;
    MyUtilities myUtilities;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);
        init();
    }

    private void init() {
        myUtilities = new MyUtilities(ActivityDaftar.this);
        setToolbarTitle(getResources().getString(R.string.title_activity_daftar));
        setToolbarAsUp(true, this);
        btnDaftar = (Button) findViewById(R.id.btnDaftar);
        txtNamaDepan = (EditText) findViewById(R.id.txtDaftarNamaDepan);
        txtNamaBelakang = (EditText) findViewById(R.id.txtDaftarNamaBelakang);
        txtEmail = (EditText) findViewById(R.id.txtDaftarEmail);
        txtPassword = (EditText) findViewById(R.id.txtDaftarPassword);
        txtUsername = (EditText) findViewById(R.id.txtDaftarUsername);
        txtNoTelp = (EditText) findViewById(R.id.txtDaftarNoTelp);
        txtAlamat = (EditText) findViewById(R.id.txtDaftarAlamat);
        txtNoRekening = (EditText) findViewById(R.id.txtDaftarNoRekening);
        txtNoKTP = (EditText) findViewById(R.id.txtDaftarNoKTP);
        txtReferal = (EditText) findViewById(R.id.txtDaftarReferal);
        txtBank = (EditText) findViewById(R.id.txtDaftarBank);
//        spinnerBank = (Spinner) findViewById(spinnerBank);

        //btnRadioLaki = (RadioButton) findViewById(R.id.btnRadioLaki);
        // btnRadioPerempuan = (RadioButton) findViewById(R.id.btnRadioPerempuan);

        btnDaftar.setOnClickListener(this);
        sessionManager = new SessionManager(getApplicationContext());

        progressDialog = new ProgressDialog(ActivityDaftar.this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view == btnDaftar) {
//            createUser();
//            showChangeLangDialog();
            checkField();
        }
    }

    private void checkField() {
        boolean cancel = false;
        View focusView = null;
        String username = txtUsername.getText().toString().trim();
        String namaDepan = txtNamaDepan.getText().toString().trim();
        String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();
        String namaBelakang = txtNamaBelakang.getText().toString().trim();
        String noTelp = txtNoTelp.getText().toString().trim();
        String noKTP = txtNoKTP.getText().toString().trim();
        String noRekening = txtNoRekening.getText().toString().trim();
        String alamat = txtAlamat.getText().toString().trim();
        String referal = txtReferal.getText().toString().trim();
        String bank = txtBank.getText().toString().trim();
        if (TextUtils.isEmpty(namaDepan)) {
            txtNamaDepan.setError("Tidak boleh kosong");
            focusView = txtNamaDepan;
            cancel = true;
        } else if (TextUtils.isEmpty(username)) {
            txtUsername.setError("Tidak boleh kosong");
            focusView = txtUsername;
            cancel = true;
        } else if (TextUtils.isEmpty(email) || !isEmailValid(email)) {
            txtEmail.setError("Email tidak valid");
            focusView = txtEmail;
            cancel = true;
        } else if (TextUtils.isEmpty(password) || !isPasswordValid(password)) {
            txtPassword.setError("Tidak boleh kosong dan minimal 6 karakter");
            focusView = txtPassword;
            cancel = true;
        } else if (TextUtils.isEmpty(noTelp) || noTelp.length() < 11) {
            txtNoTelp.setError("Tidak boleh kosong dan minimal 11");
            focusView = txtNoTelp;
            cancel = true;
        } else if (TextUtils.isEmpty(alamat) || alamat.length() < 10) {
            txtAlamat.setError("Tidak boleh kosong dan minimal 10 karakter");
            focusView = txtAlamat;
            cancel = true;
        } else if (TextUtils.isEmpty(bank)) {
            txtBank.setError("Tidak boleh kosong");
            focusView = txtBank;
            cancel = true;
        } else if (TextUtils.isEmpty(noRekening) || noRekening.length() < 8) {
            txtNoRekening.setError("Isi No. Rekening dengan benar");
            focusView = txtNoRekening;
            cancel = true;
        } else if (TextUtils.isEmpty(noKTP) || noKTP.length() < 16) {
            txtNoKTP.setError("Isi No KTP dengan benar");
            focusView = txtNoKTP;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
//            Toast.makeText(this, "YEAY", Toast.LENGTH_SHORT).show();
//            createUser();
//            showInfoDialog();
            if (TextUtils.isEmpty(txtReferal.getText().toString().trim())) {
                checkReferalDialog();
            } else {
                checkUsernameReferal(referal);
            }
        }
    }

    private boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                .matches();
    }

    private boolean isPasswordValid(String password) {

        return password.length() > 5;
    }

    private void createUser() {
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        String username = txtUsername.getText().toString().trim();
        String namaDepan = txtNamaDepan.getText().toString().trim();
        String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();
        String namaBelakang = txtNamaBelakang.getText().toString().trim();
        String noTelp = txtNoTelp.getText().toString().trim();
        String noKTP = txtNoKTP.getText().toString().trim();
        String noRekening = txtNoRekening.getText().toString().trim();
        String alamat = txtAlamat.getText().toString().trim();
        String referal = txtReferal.getText().toString().trim();
        String bank = txtBank.getText().toString().trim();
        if (TextUtils.isEmpty(referal)) {
            referal = "";
        }

        PelangganInterface apiService = Communicator.getClient().create(PelangganInterface.class);
        Call<UserModel> user = apiService.createUser(username, namaDepan, namaBelakang,
                noTelp, alamat, bank, noRekening, noKTP, referal, email, password);
        user.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    UserModel model = response.body();
                    if (model != null) {
                        if (model.isError())
                            Toast.makeText(ActivityDaftar.this, model.getMessage(), Toast.LENGTH_SHORT).show();
                        else {
                            showInfoDialog();
                        }
                    } else {
                        Toast.makeText(ActivityDaftar.this, "Terjadi kesalahan...Coba Lagi", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                progressDialog.dismiss();
//                Toast.makeText(ActivityDaftar.this, "Failed to connect the server", Toast.LENGTH_SHORT).show();
                Toast.makeText(ActivityDaftar.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void checkReferalDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityDaftar.this, R.style.MyAlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle("Konfirmasi");
        builder.setMessage("Referal anda kosong, lanjut daftar?");
        builder.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        createUser();
                    }
                });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showInfoDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        dialogBuilder.setTitle("Informasi");
        dialogBuilder.setMessage("Registrasi berhasil, \nSilahkan cek email anda untuk verifikasi\nTerima Kasih");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                startActivity(new Intent(ActivityDaftar.this, MainActivity.class));
                overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
                finish();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.setCancelable(false);
        b.setCanceledOnTouchOutside(false);
        b.show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(ActivityDaftar.this, MainActivity.class));
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        super.onBackPressed();
    }

    public void checkUsernameReferal(String username) {
        PelangganInterface service = Communicator.getClient().create(PelangganInterface.class);
        service.usernameIsValid(username).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.isSuccessful()) {

                    if (response.body().isError()) {
                        myUtilities.showInfoDialog(response.body().getMessage(), R.style.MyAlertDialogStyle, ActivityDaftar.this, false);
                    } else {
                        createUser();
                    }
                }

            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                myUtilities.showInfoDialog("Failed to connect the server", R.style.MyAlertDialogStyle, ActivityDaftar.this, false);

            }
        });
    }


}
