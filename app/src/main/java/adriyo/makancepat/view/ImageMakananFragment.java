package adriyo.makancepat.view;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.ImageMakananAdapter;
import adriyo.makancepat.model.GaleriMakananResponse;
import adriyo.makancepat.model.ImageMakananModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.GaleriMakananInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageMakananFragment extends Fragment {

    private ViewPager mViewPager;
    private ImageMakananAdapter adapterView;
    private int idMenu;
    private List<ImageMakananModel> data;
    public ImageMakananFragment() {
        // Required empty public constructor
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            idMenu = bundle.getInt("idMenu", 0);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        requestData(idMenu);
        View rootView = inflater.inflate(R.layout.fragment_image_makanan, container, false);
        mViewPager = (ViewPager) rootView.findViewById(R.id.viewPagerMenuMakanan);
        adapterView = new ImageMakananAdapter(getActivity(), data);
        mViewPager.setAdapter(adapterView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTab();
    }

    private void setTab() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int position) {
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageSelected(int position) {
            }

        });
    }

    public void requestData(int idMenu) {
        GaleriMakananInterface apiService = Communicator.getClient().create(GaleriMakananInterface.class);
        Call<GaleriMakananResponse> call = apiService.getGaleriMakanan(idMenu);
        call.enqueue(new Callback<GaleriMakananResponse>() {
            @Override
            public void onResponse(Call<GaleriMakananResponse> call, Response<GaleriMakananResponse> response) {
                if (response.isSuccessful())
                    data = response.body().getList();
            }

            @Override
            public void onFailure(Call<GaleriMakananResponse> call, Throwable t) {
                // Log error here since request failed
                Toast.makeText(getActivity(), "Failed to request data", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
