package adriyo.makancepat.view;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.MenuRestoAdapter;
import adriyo.makancepat.model.MenuRestoModel;
import adriyo.makancepat.model.MenuRestoResponse;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.MenuRestoInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MinumanRestoFragment extends Fragment {

    List<MenuRestoModel> listMenu;
    RecyclerView recyclerView;
    private int mColumnCount = 2;
    Context context;
    String id;


    public MinumanRestoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_minuman_resto, container, false);

        context = view.getContext();
        Bundle extras = getActivity().getIntent().getExtras();
        id = extras.getString("idResto");

        recyclerView = (RecyclerView) view.findViewById(R.id.rvMenuMakanan);
        recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        //setData();

        MenuRestoInterface apiService = Communicator.getClient().create(MenuRestoInterface.class);

        Call<MenuRestoResponse> call = apiService.getMinumanResto(Integer.parseInt(id));
        call.enqueue(new Callback<MenuRestoResponse>() {
            @Override
            public void onResponse(Call<MenuRestoResponse> call, Response<MenuRestoResponse> response) {
                listMenu = response.body().getList();
                recyclerView.setAdapter(new MenuRestoAdapter(listMenu, getActivity()));
                //Toast.makeText(getActivity().getApplicationContext(), "sukses : "+response.code(), Toast.LENGTH_SHORT).show();
                //Toast.makeText(getActivity().getApplicationContext(), "id resto : "+id, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MenuRestoResponse> call, Throwable t) {
                // Log error here since request failed
                Toast.makeText(getActivity().getApplicationContext(), "failed : "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

}
