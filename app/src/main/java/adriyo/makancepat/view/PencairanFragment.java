package adriyo.makancepat.view;


import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.ErrorUtils;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.InfoModel;
import adriyo.makancepat.model.SaldoModel;
import adriyo.makancepat.model.UserModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.PelangganInterface;
import adriyo.makancepat.rest.TransaksiInterface;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PencairanFragment extends Fragment {

    ProgressDialog progressDialog;
    SessionManager sessionManager;
    MyUtilities myUtilities;
    EditText txtNoRekening, txtBesaran;
    TextView txtWarning;
    Button btnKirim;
    String idUser = "";
    private int MINIMAL_SALDO = 100000;

    public PencairanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        showMessage(false, "");
        showMessage(true, "");
        cekSaldo(idUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pencairan, container, false);
        WebView webView = (WebView) view.findViewById(R.id.webView);
        progressDialog = new ProgressDialog(getActivity());
        sessionManager = new SessionManager(getActivity());
        myUtilities = new MyUtilities(getActivity());
        idUser = sessionManager.getUserDetails().get(SessionManager.KEY_ID_USER);
        String informasi = getActivity().getResources().getString(R.string.info_pencairan);
        webView.loadDataWithBaseURL(null, informasi, "text/html", "utf-8", null);
        txtNoRekening = (EditText) view.findViewById(R.id.txtNoRekening);
        txtBesaran = (EditText) view.findViewById(R.id.txtNominalPencairan);
        btnKirim = (Button) view.findViewById(R.id.btnPencairanKirim);
        txtWarning = (TextView) view.findViewById(R.id.pencairanLabelWarning);
        String email = sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL);
        if (Build.VERSION.SDK_INT >= 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        btnKirim.setOnClickListener(view1 -> {
            txtBesaran.setError(null);
            if (!TextUtils.isEmpty(txtBesaran.getText().toString().trim())) {
                String idUser1 = sessionManager.getUserDetails().get(SessionManager.KEY_ID_USER);
                String besaran = txtBesaran.getText().toString().trim();
                postData(idUser1, besaran);
            } else {
                txtBesaran.setError("Tidak boleh kosong");
                txtBesaran.requestFocus();
            }
        });

        requestDataUser(txtNoRekening, email);
        return view;
    }

    public void requestDataUser(final EditText txtNoRekening, String email) {
        progressDialog.setMessage("Processing...");
        progressDialog.show();
        PelangganInterface service = Communicator.getClient().create(PelangganInterface.class);
        service.getUser(email).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    txtNoRekening.setText(response.body().getNoRekening());
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                progressDialog.dismiss();

            }
        });
    }

    public void postData(String idUser, String besaran) {
        progressDialog.setMessage("Processing...");
        progressDialog.show();
        TransaksiInterface service = Communicator.getClient().create(TransaksiInterface.class);
        service.postPencairan(idUser, besaran).enqueue(new Callback<InfoModel>() {
            @Override
            public void onResponse(Call<InfoModel> call, Response<InfoModel> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    myUtilities.showInfoDialog(response.body().getMessage(), R.style.MyAlertDialogStyle, getActivity(), false);
                }
            }

            @Override
            public void onFailure(Call<InfoModel> call, Throwable t) {
                progressDialog.dismiss();
                myUtilities.showInfoDialog("Terjadi kesalahan", R.style.MyAlertDialogStyle, getActivity(), false);

            }
        });
    }

    public void cekSaldo(String idUser) {
        PelangganInterface service = Communicator.getClient().create(PelangganInterface.class);
        Observable<SaldoModel> saldoModel = service.getSaldo(Integer.parseInt(idUser));
        saldoModel.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(saldoModel1 -> {
                            if (saldoModel1.getSaldo() > MINIMAL_SALDO) {
                                showMessage(false, "");
                            } else {
                                showMessage(true, "Saldo anda tidak cukup");
                            }
                        }, throwable -> {
                            showMessage(true, ErrorUtils.getErrorMessage(throwable));

                        }
                );

    }

    public void showMessage(boolean bol, String teks) {
        if (bol) {
            txtWarning.setText(teks);
            txtWarning.setVisibility(View.VISIBLE);
            txtNoRekening.setEnabled(false);
            txtBesaran.setEnabled(false);
            btnKirim.setEnabled(false);
        } else {
            txtWarning.setText(teks);
            txtWarning.setVisibility(View.GONE);
            txtNoRekening.setEnabled(true);
            txtBesaran.setEnabled(true);
            btnKirim.setEnabled(true);
        }

    }


}
