package adriyo.makancepat.view.myDialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.KurirModel;
import adriyo.makancepat.model.PemasukanAwalModel;
import adriyo.makancepat.model.SaldoModel;
import adriyo.makancepat.model.TransaksiModel;
import adriyo.makancepat.model.UserModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.PelangganInterface;
import adriyo.makancepat.rest.TransaksiInterface;
import adriyo.makancepat.view.CheckoutCompletedActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author adriyo
 *         Created by ADMIN on 1/22/2017.
 */

public class KurirInfoDialog extends DialogFragment {


    @BindView(R.id.txtNamaKurir)
    TextView namaKurirTv;
    @BindView(R.id.txtPhoneKurir)
    TextView telpKurirTv;
    @BindView(R.id.labelRating)
    TextView labelRating;

    @BindView(R.id.imgKurir)
    CircleImageView imgKurir;

    @BindView(R.id.ratingKurir)
    AppCompatRatingBar ratingKurir;

    @BindView(R.id.rgMetodePembayaran)
    RadioGroup rgMetodePembayaran;

    UserModel userModel;
    UserModel userPemesanModel;//model user untuk data pemesan
    KurirModel kurirModel;
    PemasukanAwalModel pemasukanModel;

    TransaksiModel model;
    @BindView(R.id.btnMetodeCash)
    AppCompatRadioButton btnCash;

    @BindView(R.id.btnMetodeSaldo)
    AppCompatRadioButton btnSaldo;

    @BindView(R.id.txtSaldo)
    EditText txtSaldo;
    MyUtilities utilities;

    ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.info_kurir_dialog, container, false);
        ButterKnife.bind(this, view);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyDialogNoTitle);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
        SessionManager sessionManager = new SessionManager(getActivity());
        model = new TransaksiModel();
        kurirModel = new KurirModel();
        kurirModel = (KurirModel) getArguments().getSerializable("kurir_model");

        userPemesanModel = new UserModel();
        userPemesanModel = (UserModel) getArguments().getSerializable("user_pemesan_model");

        userModel = new UserModel();
        pemasukanModel = new PemasukanAwalModel();
        userModel.setUsername(sessionManager.getUserDetails().get(SessionManager.KEY_USERNAME));
        userModel.setIdUser(sessionManager.getUserDetails().get(SessionManager.KEY_ID_USER));
        model.setUserModel(userModel);
        model.setIdTransaksi(getArguments().getInt("id_transaksi"));
        kurirModel.setIdKurir(getArguments().getInt("id_kurir"));
        model.setKurirModel(kurirModel);
        model.setTotalHarga(getArguments().getInt("total_harga"));
        model.setTotalTransaksi(getArguments().getInt("total_transaksi"));
        model.setLatitude(getArguments().getString("latitude"));
        model.setLongitude(getArguments().getString("longitude"));
        pemasukanModel.setIdPemasukan(getArguments().getInt("id_pemasukan"));
        int idKurir = getArguments().getInt("id_kurir");
        String nama = getArguments().getString("nama");
        String phone = getArguments().getString("phone");
        String foto = getArguments().getString("foto");
        int totalHarga = getArguments().getInt("total_harga");
        String latitude = getArguments().getString("latitude");
        String longitude = getArguments().getString("longitude");
        float ratingVal = getArguments().getFloat("rate");
        int isValidate = getArguments().getInt("is_validate");


        Button btnSetujuTransasi = (Button) view.findViewById(R.id.btnSetujuTransaksi);


        ratingKurir.setRating(ratingVal);
        namaKurirTv.setText(nama);
        telpKurirTv.setText(phone);
        utilities = new MyUtilities();

        if (ratingVal == 0)
            labelRating.setVisibility(View.VISIBLE);
        else labelRating.setVisibility(View.GONE);

        btnSaldo.setEnabled(false);
        getSaldo(Integer.parseInt(sessionManager.getUserDetails().get(SessionManager.KEY_ID_USER)));

        String url = Communicator.BASE_URL + foto;
        Picasso.with(getActivity()).load(url).placeholder(R.mipmap.ic_launcher).into(imgKurir);
        btnSetujuTransasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int radioButtonID = rgMetodePembayaran.getCheckedRadioButtonId();
                View radioButton = rgMetodePembayaran.findViewById(radioButtonID);
                int idx = rgMetodePembayaran.indexOfChild(radioButton);
                String tmp = txtSaldo.getText().toString().trim();
                int saldo = 0;
                if (idx == 0) {
                    postTransaksi(0);
                } else {
                    postTransaksi(1);
                }

            }
        });

        rgMetodePembayaran.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                View radioButton = rgMetodePembayaran.findViewById(checkedId);
                int index = rgMetodePembayaran.indexOfChild(radioButton);

                if (index == 1) {
                    if (model.getBayarSaldo() < model.getTotalTransaksi()) {
                        utilities.showInfoDialog("Maaf saldo anda tidak cukup", R.style.MyAlertDialogStyle, getActivity(), false);
                        btnCash.setChecked(true);
                    } else dialogUseSaldo();
                } else {
                    txtSaldo.setEnabled(false);
                }
            }
        });

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    private void postTransaksi(int index) {
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        TransaksiInterface apiServiceTransaksi = Communicator.getClient().create(TransaksiInterface.class);
        Call<TransaksiModel> callTransaksi = null;
        if (index == 0) {
            callTransaksi = apiServiceTransaksi.checkoutTransaksi(
                    model.getIdTransaksi(),
                    model.getKurirModel().getIdKurir(),
                    model.getTotalTransaksi(),
                    model.getLatitude(),
                    model.getLongitude(),
                    model.getUserModel().getIdUser(),
                    model.getUserModel().getUsername(),
                    model.getTotalHarga(),
                    pemasukanModel.getIdPemasukan(),
                    userPemesanModel.getFirstName(),
                    userPemesanModel.getPhone(),
                    userPemesanModel.getAlamat()

            );
        } else {
            int minusSaldo = model.getTotalTransaksi();
            callTransaksi = apiServiceTransaksi.checkoutTransaksiSaldo(
                    model.getIdTransaksi(),
                    model.getKurirModel().getIdKurir(),
                    model.getTotalTransaksi(),
                    model.getLatitude(),
                    model.getLongitude(),
                    model.getUserModel().getIdUser(),
                    model.getUserModel().getUsername(),
                    model.getTotalHarga(),
                    minusSaldo,
                    pemasukanModel.getIdPemasukan(),
                    userPemesanModel.getFirstName(),
                    userPemesanModel.getPhone(),
                    userPemesanModel.getAlamat()
            );
        }

        callTransaksi.enqueue(new Callback<TransaksiModel>() {
            @Override
            public void onResponse(Call<TransaksiModel> call, Response<TransaksiModel> response) {
                progressDialog.dismiss();
                if (response.isSuccessful() && !response.body().isError()) {
//                    Toast.makeText(CheckoutActivity.this, "Success", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                    Intent intent = new Intent(getActivity(), CheckoutCompletedActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);

                } else {
                    utilities.showInfoDialog(response.body().getMessage(), R.style.MyAlertDialogStyle, getActivity(), false);
                }
            }

            @Override
            public void onFailure(Call<TransaksiModel> call, Throwable t) {
                progressDialog.dismiss();
                dismiss();
//                utilities.showInfoDialog("Failed to process data", R.style.MyAlertDialogStyle, getActivity(), false);
                utilities.showInfoDialog(t.getMessage(), R.style.MyAlertDialogStyle, getActivity(), false);
            }
        });
    }

    private void getSaldo(int idUser) {
        PelangganInterface api = Communicator.getClient().create(PelangganInterface.class);
        Call<SaldoModel> call = api.saldoPelanggan(idUser);
        call.enqueue(new Callback<SaldoModel>() {
            @Override
            public void onResponse(Call<SaldoModel> call, Response<SaldoModel> response) {
                if (response.isSuccessful()) {
                    if (!response.body().isError()) {
                        btnSaldo.setEnabled(true);
                        SaldoModel saldoModel = response.body();
                        model.setBayarSaldo(saldoModel.getSaldo());
                        txtSaldo.setText(saldoModel.getSaldo() + "");
                    }
                }
            }

            @Override
            public void onFailure(Call<SaldoModel> call, Throwable t) {

            }
        });
    }

    private void dialogUseSaldo() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
        alertDialogBuilder.setTitle("Konfirmasi");
        alertDialogBuilder
                .setMessage("Ingin menggunakan saldo anda?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        btnCash.setChecked(true);
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

    }

}
