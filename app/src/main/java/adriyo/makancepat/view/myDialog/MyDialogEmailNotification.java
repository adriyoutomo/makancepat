package adriyo.makancepat.view.myDialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import adriyo.makancepat.R;

/**
 * Created by ADMIN on 8/30/2016.
 */

public class MyDialogEmailNotification extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_email_notification, null);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyDialogNoTitle);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

//        String title = getArguments().getString("title");
//        String overview = getArguments().getString("overview");
//        TextView txtTitle = (TextView) view.findViewById(R.id.id);
//        TextView txtTitle2 = (TextView) view.findViewById(R.id.content);
//        txtTitle.setText(title);
//        txtTitle2.setText(overview);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }
}
