package adriyo.makancepat.view;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.UserModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.PelangganInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener, TextWatcher {

    private Spinner spinner;
    private Button btnEdit;
    private EditText txtNamaDepan, txtNamaBelakang, txtNoKTP, txtPhone, txtNoRekening, txtAlamat;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (getSupportActionBar() == null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Edit Profile");
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }
            });
        }

        init();
        requestData(sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL));
        checkFieldsForEmptyValues();
    }

    private void init() {
        sessionManager = new SessionManager(EditProfileActivity.this);

        txtNamaDepan = (EditText) findViewById(R.id.txtNamaDepan);
        txtNamaBelakang = (EditText) findViewById(R.id.txtNamaBelakang);
        txtPhone = (EditText) findViewById(R.id.txtNoTelp);
        txtNoKTP = (EditText) findViewById(R.id.txtNoKTP);
        txtNoRekening = (EditText) findViewById(R.id.txtNoRekening);
        txtAlamat = (EditText) findViewById(R.id.txtAlamat);

        txtNamaDepan.addTextChangedListener(this);
        txtNamaBelakang.addTextChangedListener(this);
        txtPhone.addTextChangedListener(this);
        txtNoKTP.addTextChangedListener(this);
        txtNoRekening.addTextChangedListener(this);
        txtAlamat.addTextChangedListener(this);

        spinner = (Spinner) findViewById(R.id.spinnerBank);
        spinner.setOnItemSelectedListener(this);

        btnEdit = (Button) findViewById(R.id.btnEditProfil);
        btnEdit.setOnClickListener(this);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View view) {
        if (view == btnEdit) {
            updateService();
        }
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        checkFieldsForEmptyValues();
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void checkFieldsForEmptyValues() {
        String namaDepan = txtNamaDepan.getText().toString();
        String noTelp = txtPhone.getText().toString();
        String noKTP = txtNoKTP.getText().toString();
        String noRekening = txtNoRekening.getText().toString();
        String alamat = txtAlamat.getText().toString();
        if (namaDepan.length() > 3 && noKTP.length() > 12
                && noTelp.length() > 5 && noRekening.length() > 9 && alamat.length() > 10) {
            btnEdit.setEnabled(true);
        } else {
            btnEdit.setEnabled(false);
        }
    }


    private void updateService() {
        String idUser = sessionManager.getUserDetails().get(SessionManager.KEY_ID_USER);
        String namaDepan = txtNamaDepan.getText().toString();
        String namaBelakang = txtNamaBelakang.getText().toString();
        String noTelp = txtPhone.getText().toString();
        String noKTP = txtNoKTP.getText().toString();
        String noRekening = txtNoRekening.getText().toString();
        String alamat = txtAlamat.getText().toString();
        String bank = spinner.getSelectedItem().toString();
        PelangganInterface service = Communicator.getClient().create(PelangganInterface.class);
        service.updateProfilUser(idUser, namaDepan, namaBelakang, noTelp, noKTP, noRekening, alamat, bank)
                .enqueue(new Callback<UserModel>() {
                    @Override
                    public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                        if (response.isSuccessful()) {
                            Intent intent = new Intent();
                            intent.putExtra("message", response.body().getMessage());
                            setResult(RESULT_OK, intent);
                            finish();
                            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        }
                    }

                    @Override
                    public void onFailure(Call<UserModel> call, Throwable t) {
                        setResult(RESULT_CANCELED);
                        finish();
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

                    }
                });
    }

    private void requestData(String email) {
        PelangganInterface service = Communicator.getClient().create(PelangganInterface.class);
        service.getUser(email).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.isSuccessful()) {
                    UserModel model = response.body();
                    txtNamaDepan.setText(model.getFirstName());
                    txtNamaBelakang.setText(model.getLastName());
                    txtPhone.setText(model.getPhone());
                    txtNoKTP.setText(model.getNoKtp());
                    txtNoRekening.setText(model.getNoRekening());
                    txtAlamat.setText(model.getAlamat());

                    Resources res = getResources();
                    String[] planets = res.getStringArray(R.array.list_bank);
                    int i = 0;
                    for (String text : planets) {
                        if (model.getBank().equals(text)) {
                            spinner.setSelection(i);
                            return;
                        }
                        i++;
                    }
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {

            }
        });
    }
}
