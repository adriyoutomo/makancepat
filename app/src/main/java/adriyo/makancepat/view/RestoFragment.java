package adriyo.makancepat.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.RestoRecyclerViewAdapter;
import adriyo.makancepat.customutil.RecyclerTouchListener;
import adriyo.makancepat.model.RestoModel;
import adriyo.makancepat.model.RestoResponse;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.RestoInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class RestoFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private int mColumnCount = 2;
    private OnListFragmentInteractionListener mListener;
    List<RestoModel> listResto;
    RecyclerView recyclerView;
    SwipeRefreshLayout refreshLayout;
    private LinearLayout errorLayout;
    private TextView txtError;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RestoFragment() {
    }

    @SuppressWarnings("unused")
    public static RestoFragment newInstance() {
        RestoFragment fragment = new RestoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_resto, container, false);

        // Set the adapter
        Context context = view.getContext();
        recyclerView = (RecyclerView) view.findViewById(R.id.rvResto);
        recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.fragmentRefreshResto);
        refreshLayout.setOnRefreshListener(this);

        errorLayout = (LinearLayout) view.findViewById(R.id.errorLayout);
        txtError = (TextView) view.findViewById(R.id.txtErrorInformation);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(context, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int posisi) {
                RestoModel model = listResto.get(posisi);
                Intent intent = new Intent(getActivity(), DetailRestoActivity.class);
                intent.putExtra("idResto", model.getIdWarung()+"");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

            }
        }));

        ((HomeActivity) getActivity()).setMyTitleBar("Resto");
        showErrorLayout(true);
        requestData();
        return view;
    }

    public void requestData(){
        RestoInterface apiService = Communicator.getClient().create(RestoInterface.class);
        // TODO auth key
        Call<RestoResponse> call = apiService.getResto("");
        call.enqueue(new Callback<RestoResponse>() {
            @Override
            public void onResponse(Call<RestoResponse> call, Response<RestoResponse> response) {
                refreshLayout.setRefreshing(false);
                showErrorLayout(false);
                if (response.isSuccessful()) {
                    listResto = response.body().getListResto();
                    recyclerView.setAdapter(new RestoRecyclerViewAdapter(listResto, mListener));
                }
            }

            @Override
            public void onFailure(Call<RestoResponse> call, Throwable t) {
                refreshLayout.setRefreshing(false);
                showErrorLayout(true);
                listResto = new ArrayList<RestoModel>();
                recyclerView.setAdapter(new RestoRecyclerViewAdapter(listResto, mListener));
                txtError.setText("Failed to request data");
                // Log error here since request failed
//                Toast.makeText(getActivity(), "Failed to request data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        if(refreshLayout!=null){
            refreshLayout.setRefreshing(false);
            refreshLayout.destroyDrawingCache();
            refreshLayout.clearAnimation();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        requestData();
    }

    public void showErrorLayout(boolean show){
        if (show) {
            recyclerView.setVisibility(View.GONE);
            errorLayout.setVisibility(View.VISIBLE);
        }else{
            recyclerView.setVisibility(View.VISIBLE);
            errorLayout.setVisibility(View.GONE);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(RestoModel item);
    }
}
