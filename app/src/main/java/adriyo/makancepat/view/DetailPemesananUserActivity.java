package adriyo.makancepat.view;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.DetailPesananUserRVAdapter;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.model.DetailPesananUserModel;
import adriyo.makancepat.model.DetailPesananUserResponse;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.DaftarPesananUserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPemesananUserActivity extends AppCompatActivity implements View.OnClickListener {

    int idTransaksi = 0;
    private RecyclerView recyclerView;
    private List<DetailPesananUserModel> data;
    private ProgressBar progressBar;
    private MyUtilities myUtilities;
    MyUtilities x = new MyUtilities();
    private Button btnDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pemesanan_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (getSupportActionBar() == null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Detail Pemesanan");
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //NavUtils.navigateUpFromSameTask(CheckoutActivity.this);
                    finish();
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

                }
            });
        }
        init();
        idTransaksi = getIntent().getExtras().getInt("idTransaksi");
        myUtilities.showLoading(progressBar, true);
        enableButton(false);
        requestData(idTransaksi);
    }

    private void init() {
        recyclerView = (RecyclerView) findViewById(R.id.rvDetailPemesananUser);
        progressBar = (ProgressBar) findViewById(R.id.pbDetailPembelian);
        myUtilities = new MyUtilities();

        btnDialog = (Button) findViewById(R.id.btnDialogRating);
        btnDialog.setOnClickListener(this);
    }

    private void requestData(int idTransaksi) {
        DaftarPesananUserInterface apiService = Communicator.getClient().create(DaftarPesananUserInterface.class);

        Call<DetailPesananUserResponse> call = apiService.getDetailDaftarPesanan(idTransaksi);
        call.enqueue(new Callback<DetailPesananUserResponse>() {
            @Override
            public void onResponse(Call<DetailPesananUserResponse> call, Response<DetailPesananUserResponse> response) {
                myUtilities.showLoading(progressBar, false);
                if (response.isSuccessful()) {
                    data = response.body().getListMenu();
                    recyclerView.setAdapter(new DetailPesananUserRVAdapter(data));
                    if (data.get(0).getIsShip() == 1) enableButton(true);
                } else {
                    Log.i("errBeliFragment", response.message());
                }
            }

            @Override
            public void onFailure(Call<DetailPesananUserResponse> call, Throwable t) {
                myUtilities.showLoading(progressBar, false);
            }
        });
    }

    public void enableButton(boolean val) {
        if (val) {
            btnDialog.setEnabled(true);
            btnDialog.setText("Beri Rating");
        } else {
            btnDialog.setEnabled(false);
            btnDialog.setText("Tunggu pesanan sampai terlebih dahulu...");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (null == data) return;
        if (requestCode == RESULT_ADD_RATING && resultCode == Activity.RESULT_OK) {
            x.showInfoDialog(data.getStringExtra("message"), R.style.MyAlertDialogStyle, this, false);
        }

    }

    private int RESULT_ADD_RATING = 1001;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }


    @Override
    public void onClick(View view) {
        if (view == btnDialog) {
            Intent intent = new Intent(DetailPemesananUserActivity.this, CompleteTransaksiActivity.class);
            intent.putExtra("id_transaksi", idTransaksi);
            startActivityForResult(intent, RESULT_ADD_RATING);
        }
    }
}
