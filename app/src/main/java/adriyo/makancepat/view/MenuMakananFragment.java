package adriyo.makancepat.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.MenuMakananRecyclerViewAdapter;
import adriyo.makancepat.customutil.RecyclerTouchListener;
import adriyo.makancepat.model.MenuMakananModel;
import adriyo.makancepat.model.MenuMakananResponse;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.MenuMakananInterface;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MenuMakananFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MenuMakananFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenuMakananFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    List<MenuMakananModel> listMenu;
    @BindView(R.id.rvMenuMakanan)
    RecyclerView recyclerView;
    private int mColumnCount = 2;
    Context context;

    public MenuMakananFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MenuMakananFragment.
     */
    public static MenuMakananFragment newInstance() {
        return new MenuMakananFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu_makanan, container, false);
        ButterKnife.bind(this, view);

        //Set Fragment's name
        ((HomeActivity) getActivity()).setMyTitleBar("Menu Makanan");

        // Set the adapter
        context = view.getContext();
        recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));

        MenuMakananInterface apiService = Communicator.getClient().create(MenuMakananInterface.class);

        // TODO auth key
        Call<MenuMakananResponse> call = apiService.getMenuMakanan("");
        call.enqueue(new Callback<MenuMakananResponse>() {
            @Override
            public void onResponse(Call<MenuMakananResponse> call, Response<MenuMakananResponse> response) {
                if (response.isSuccessful()) {
                    listMenu = response.body().getListMenu();
                    recyclerView.setAdapter(new MenuMakananRecyclerViewAdapter(listMenu, mListener, getActivity()));
                }
            }

            @Override
            public void onFailure(Call<MenuMakananResponse> call, Throwable t) {
                // Log error here since request failed
                //Toast.makeText(getActivity().getApplicationContext(), "Failed to request data", Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(context, recyclerView, (view1, posisi) -> {
            MenuMakananModel model = listMenu.get(posisi);
            Intent intent = new Intent(getActivity(), DetailMakananActivity.class);
            intent.putExtra("idMakanan", model.getIdMenu());
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

        }));
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(MenuMakananModel item);
    }
}
