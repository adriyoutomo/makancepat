package adriyo.makancepat.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import adriyo.makancepat.R;
import adriyo.makancepat.model.KurirModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.KurirInterface;
import adriyo.makancepat.rest.TransaksiInterface;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompleteTransaksiActivity extends AppCompatActivity {

    AppCompatRatingBar ratingBar;
    Button btnAddRating;
    int idTransaksi = 0;
    CircleImageView imgKurir;
    TextView txtNamaKurir, txtPhoneKurir, txtNote;
    private ProgressDialog progressdialog;
    KurirModel kurirModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_transaksi);
        init();
        getDataKurir();
    }

    private void init() {
        kurirModel = new KurirModel();
        ratingBar = (AppCompatRatingBar) findViewById(R.id.ratingKurir);
        btnAddRating = (Button) findViewById(R.id.btnAddRating);

        txtNamaKurir = (TextView) findViewById(R.id.txtNamaKurir);
        txtPhoneKurir = (TextView) findViewById(R.id.txtPhoneKurir);
        txtNote = (TextView) findViewById(R.id.txtNoteKurir);
        imgKurir = (CircleImageView) findViewById(R.id.imgKurir);

        progressdialog = new ProgressDialog(CompleteTransaksiActivity.this);
        progressdialog.setMessage("Please wait");
        progressdialog.setCanceledOnTouchOutside(false);
        progressdialog.setCancelable(false);

        idTransaksi = getIntent().getExtras().getInt("id_transaksi");

    }

    public void AddRating(View view) {
        float rating = ratingBar.getRating();
        String note = txtNote.getText().toString().trim();
        addRatingKurir(rating, note);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    private void getDataKurir(){
        progressdialog.show();
        TransaksiInterface api = Communicator.getClient().create(TransaksiInterface.class);
        Call<KurirModel> call =  api.getKurirByIDTransaksi(idTransaksi);
        call.enqueue(new Callback<KurirModel>() {
            @Override
            public void onResponse(Call<KurirModel> call, Response<KurirModel> response) {
                if (response.isSuccessful()){
                    kurirModel = response.body();
                    txtNamaKurir.setText(kurirModel.getFirstName() + " "+kurirModel.getLastName());
                    txtPhoneKurir.setText(kurirModel.getPhone());
                    String url = Communicator.BASE_URL+"/kurir/images/profile/" + kurirModel.getFoto();
                    Picasso.with(CompleteTransaksiActivity.this)
                            .load(url)
                            .placeholder(R.drawable.default_user_icon)
                            .into(imgKurir);
                }
                progressdialog.dismiss();
            }

            @Override
            public void onFailure(Call<KurirModel> call, Throwable t) {

            }
        });
        progressdialog.dismiss();
    }

    private void addRatingKurir(final float rating, String note){
        KurirInterface api = Communicator.getClient().create(KurirInterface.class);
        Call<KurirModel> call = api.addRatingKurir(idTransaksi, (int) rating, kurirModel.getIdKurir(), note);
        call.enqueue(new Callback<KurirModel>() {
            @Override
            public void onResponse(Call<KurirModel> call, Response<KurirModel> response) {
                if(response.isSuccessful()){
                    Intent intent = new Intent();
                    String message = "Anda memberikan rating: " + rating;
                    intent.putExtra("message", message);
                    setResult(RESULT_OK, intent);
                    finish();
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }
            }

            @Override
            public void onFailure(Call<KurirModel> call, Throwable t) {
                Intent intent = new Intent();
                String message = "Terjadi kesalahan...";
                intent.putExtra("message", message);
                setResult(RESULT_OK, intent);
                finish();
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
    }

}
