package adriyo.makancepat.view;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.HistoryTopUpRVAdapter;
import adriyo.makancepat.customutil.ErrorUtils;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.Model_TopUp;
import adriyo.makancepat.model.Model_TopUp_Response;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.PelangganInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvTopUp;
    private List<Model_TopUp> list = new ArrayList<>();
    private SessionManager sessionManager;
    private String idUser = "";
    HistoryTopUpRVAdapter adapter = new HistoryTopUpRVAdapter();
    private LinearLayout errorLayout;
    private TextView txtError;
    private FragmentActivity myContext;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showErrorLayout(true, "");
        requestData(idUser);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refreshTopUp);
        rvTopUp = (RecyclerView) view.findViewById(R.id.rvDetailTopUp);

        sessionManager = new SessionManager(myContext);
        errorLayout = (LinearLayout) view.findViewById(R.id.errorLayout);
        txtError = (TextView) view.findViewById(R.id.txtErrorInformation);

        idUser = sessionManager.getUserDetails().get(SessionManager.KEY_ID_USER);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        myContext = (FragmentActivity) context;
        super.onAttach(context);
    }

    private void requestData(String idUser) {
        PelangganInterface service = Communicator.getClient().create(PelangganInterface.class);
        service.geHistoryTopUp(idUser).enqueue(new Callback<Model_TopUp_Response>() {
            @Override
            public void onResponse(Call<Model_TopUp_Response> call, Response<Model_TopUp_Response> response) {
                swipeRefreshLayout.setRefreshing(false);
                showErrorLayout(false,"");
                if (response.isSuccessful()) {
                    if (!response.body().isError()) {
                        list = response.body().getListTopUp();
                        adapter = new HistoryTopUpRVAdapter(list);
                        rvTopUp.setAdapter(adapter);
//                        Toast.makeText(myContext, "List: "+list.size(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Model_TopUp_Response> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                showErrorLayout(true, ErrorUtils.getErrorMessage(t));
//                Toast.makeText(myContext, "Failed to connect server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        if(swipeRefreshLayout!=null){
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.destroyDrawingCache();
            swipeRefreshLayout.clearAnimation();
        }
    }

    @Override
    public void onRefresh() {
        requestData(idUser);
    }
    public void showErrorLayout(boolean show, String message){
        if (show) {
            rvTopUp.setVisibility(View.GONE);
            errorLayout.setVisibility(View.VISIBLE);
            txtError.setText(message);
        }else{
            rvTopUp.setVisibility(View.VISIBLE);
            errorLayout.setVisibility(View.GONE);
        }
    }
}
