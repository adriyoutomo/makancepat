package adriyo.makancepat.view;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.ListBankAdapter;
import adriyo.makancepat.customutil.RealPathUtil;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.Model_TopUp;
import adriyo.makancepat.model.UserModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.Interface_TopUp;
import adriyo.makancepat.rest.PelangganInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class TopUpFragment extends Fragment {

    private Button Btn_Upload, btnPickImage;

    public static final int PICK_IMAGE = 100;
    private int jenisTransaksi = 0;
    private SessionManager sessionManager;
    EditText txtBesaran;
    TextView labelImage;
    private int besaran = 0;
    private String output = "";
    private ProgressDialog progressDialog;
    private UserModel userModel;
    RadioGroup radioGroup;
    private ExpandableListView expListView;
    private ListBankAdapter listBankAdapter;
    private List<String> listHeader;
    private HashMap<String, List<String>> listChild;
    private String imagePath = "";

    public TopUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_top_up, container, false);

        expListView = (ExpandableListView) view.findViewById(R.id.lvBankTopUp);
        prepareListData();
        listBankAdapter = new ListBankAdapter(getActivity(), listHeader, listChild);
        expListView.setAdapter(listBankAdapter);

        Btn_Upload = (Button) view.findViewById(R.id.UPLOAD);
        Btn_Upload.setOnClickListener(view12 -> requestDataUser(sessionManager.getUserDetails().get(sessionManager.KEY_EMAIL)));

        sessionManager = new SessionManager(getActivity());
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);

        return view;
    }

    private void prepareListData() {
        listHeader = new ArrayList<>();
        listChild = new HashMap<>();

        listHeader.add("BCA");
        listHeader.add("Mandiri");

        listChild.put(listHeader.get(0), Arrays.asList("No. Rekening BCA")); // Header, Child data
        listChild.put(listHeader.get(1), Arrays.asList("No. Rekening Mandiri")); // Header, Child data
    }

    private void UploadImage() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_top_up);

        radioGroup = (RadioGroup) dialog.findViewById(R.id.metode_pembayaran);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.pendaftaran_awal) {
                    jenisTransaksi = 1;
                }
                if (i == R.id.topup_saldo) {
                    jenisTransaksi = 2;
                }
                if (i == R.id.pembelian_pin) {
                    jenisTransaksi = 3;
                }
            }
        });

        radioGroup.check(R.id.pendaftaran_awal);
        if (userModel.getIsValidate() == 1) {
            radioGroup.getChildAt(0).setEnabled(false);
            radioGroup.check(R.id.topup_saldo);
        } else {
            radioGroup.getChildAt(0).setEnabled(true);

        }
        txtBesaran = (EditText) dialog.findViewById(R.id.txtBesaran);
        txtBesaran.setText("0");

        labelImage = (TextView) dialog.findViewById(R.id.labelNamaImage);
        btnPickImage = (Button) dialog.findViewById(R.id.btnPickImage);
        btnPickImage.setOnClickListener(view -> {
            if (Build.VERSION.SDK_INT >= 23) {
                if (checkPermission()) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_PICK);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE);
                    // Code for above or equal 23 API Oriented Device
                    // Your Permission granted already .Do next code
                } else {
                    requestPermission(); // Code for permission
                }
            } else {

                // Code for Below 23 API Oriented Device
                // Do next code

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE);
            }
        });

        Button dialogButton = (Button) dialog.findViewById(R.id.tmblUpload);
        dialogButton.setOnClickListener(v -> {
            boolean cancel = false;
            View focusView = null;
            besaran = Integer.parseInt(txtBesaran.getText().toString());
            if (TextUtils.isEmpty(txtBesaran.getText().toString()) || besaran == 0) {
                txtBesaran.setError("Wajib diisi..");
                focusView = txtBesaran;
                cancel = true;
            }

            if (cancel) {
                focusView.requestFocus();
            } else {
                if (TextUtils.isEmpty(output)) {
                    Toast.makeText(getActivity(), "Pilih gambar...", Toast.LENGTH_SHORT).show();
                } else {
                    besaran = Integer.parseInt(txtBesaran.getText().toString());
                    dialog.dismiss();
                    String idUser = sessionManager.getUserDetails().get(sessionManager.KEY_ID_USER);
                    postData(idUser, output, besaran);
                }
            }
        });
        dialog.show();
    }

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {

            Uri imageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(imageUri, filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = cursor.getString(columnIndex);
                cursor.close();
                if (!TextUtils.isEmpty(imagePath)) {
//                    Toast.makeText(this, "GALLERY_INTENT", Toast.LENGTH_SHORT).show();
                    File file = new File(imagePath);
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);
                    output = getEncoded64ImageStringFromBitmap(bitmap);
                    labelImage.setText(file.getName());
                }
            } else {
                imagePath = "";
                Toast.makeText(getActivity(), "Error...please try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PICK_IMAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }

    private String getPathFromUri(Uri uri) {
        if (Build.VERSION.SDK_INT < 11)
            return RealPathUtil.getRealPathFromURI_BelowAPI11(getContext(), uri);
        else if (Build.VERSION.SDK_INT < 19)
            return RealPathUtil.getRealPathFromURI_API11to18(getContext(), uri);
        else
            return RealPathUtil.getRealPathFromURI_API19(getContext(), uri);
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        return imgString;
    }

    public void postData(String idUser, String output, int besaran) {
        progressDialog.setMessage("Please Wait....");
        progressDialog.show();
        Interface_TopUp interfaceTopUp = Communicator.getClient().create(Interface_TopUp.class);
        Call<Model_TopUp> call = interfaceTopUp.postImage(idUser, besaran, output, jenisTransaksi);
        call.enqueue(new Callback<Model_TopUp>() {
            @Override
            public void onResponse(Call<Model_TopUp> call, Response<Model_TopUp> response) {
                progressDialog.dismiss();
                Log.d("responseOn", response.body().getMessage());
                Model_TopUp model_topUp = response.body();
                if (model_topUp != null) {
                    if (!model_topUp.isError()) {
                        final Dialog dialog = new Dialog(getContext());
                        dialog.setTitle("Request Berhasil");
                        dialog.setContentView(R.layout.dialog_top_up_sukses);
                        dialog.show();

                        dialog.setCancelable(true);
                        ImageView images = (ImageView) dialog.findViewById(R.id.sukses);
                        images.setOnClickListener(view -> dialog.dismiss());
                    } else {
                        Toast.makeText(getContext(), "Image Posted Before, Wait ADMIN Confirmation", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getContext(), "Failed With Connection null", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<Model_TopUp> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("onFailure", t.getMessage());
                Toast.makeText(getContext(), "Failed With Connection", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void requestDataUser(String email) {
        progressDialog.setMessage("Please Wait....");
        progressDialog.show();
        PelangganInterface service = Communicator.getClient().create(PelangganInterface.class);
        service.getUser(email).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    userModel = response.body();
                    UploadImage();
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Toast.makeText(getActivity(), "Read External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(getActivity()
                    , new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PICK_IMAGE);
        }
    }
}
