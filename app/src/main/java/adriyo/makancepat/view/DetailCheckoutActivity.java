package adriyo.makancepat.view;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.ArrayList;
import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.GroupByRestoRVAdapter;
import adriyo.makancepat.customutil.MyPermissionChecker;
import adriyo.makancepat.customutil.MyStatic;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.customutil.PermissionsActivity;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.DistanceModel;
import adriyo.makancepat.model.DurationModel;
import adriyo.makancepat.model.ElementsModel;
import adriyo.makancepat.model.KurirModel;
import adriyo.makancepat.model.ListCheckoutModel;
import adriyo.makancepat.model.MapInformationModel;
import adriyo.makancepat.model.RowsModel;
import adriyo.makancepat.model.TransaksiModel;
import adriyo.makancepat.model.UserModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.KurirInterface;
import adriyo.makancepat.rest.MyMapService;
import adriyo.makancepat.rest.TransaksiInterface;
import adriyo.makancepat.ui.transaksi.AlamatUserActivity;
import adriyo.makancepat.view.myDialog.KurirInfoDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailCheckoutActivity extends AppCompatActivity implements GroupByRestoRVAdapter.GroupByRestoActivityListener {

    private static final int INTENT_ALAMAT_TRANSAKSI = 2001;
    private GroupByRestoRVAdapter adapter;
    private ListCheckoutModel listMenu;
    @BindView(R.id.rvGroupByResto)
    RecyclerView groupByRestoRv;
    private SessionManager sessionManager;
    private int idUser;
    private ProgressDialog progressdialog;
    private Button btnPilihAlamat, btnLanjut;
    private MyUtilities myUtilities;
    private int PLACE_PICKER_REQUEST = 1993;

    @BindView(R.id.labelDetailCheckoutAlamat)
    TextView labelAlamat;

    @BindView(R.id.labelTotalOngkir)
    TextView labelOngkir;


    @BindView(R.id.labelTotalBayar)
    TextView labelTotalBayar;
    @BindView(R.id.labelTotalHarga)
    TextView labelTotalHarga;
    private List<Double> listOngkir = new ArrayList<>();
    PlacePicker.IntentBuilder builder;
    private String messageLoading = "Please Wait....";
    double myLatitude = 0, myLongitude = 0;
    TransaksiModel transaksiModel;
    int totalTransaksi = 0;
    int totalHarga = 0;
    int idTransaksi = 0;
    int idPemasukan = 0;
    MyPermissionChecker checker;
    private int isValidate = 100;

    private static final String[] PERMISSIONS_ACCESS_LOCATION = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
    private UserModel userPemesanModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_checkout);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (getSupportActionBar() == null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(R.string.title_activity_checkout);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //NavUtils.navigateUpFromSameTask(CheckoutActivity.this);
                    finish();
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }
            });
        }

        init();
        requestData();
    }

    private void init() {
        transaksiModel = new TransaksiModel();
        builder = new PlacePicker.IntentBuilder();

        sessionManager = new SessionManager(DetailCheckoutActivity.this);
        idUser = Integer.parseInt(sessionManager.getUserDetails().get(SessionManager.KEY_ID_USER));

        groupByRestoRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        groupByRestoRv.setHasFixedSize(true);

        progressdialog = new ProgressDialog(DetailCheckoutActivity.this);
        progressdialog.setMessage(messageLoading);
        progressdialog.setCanceledOnTouchOutside(false);
        progressdialog.setCancelable(false);

//        btnPilihAlamat.setVisibility(View.GONE);

        myUtilities = new MyUtilities();
        checker = new MyPermissionChecker(this);

        userPemesanModel = new UserModel();
    }

    public void requestData() {
        showDialog(true, messageLoading);
        TransaksiInterface apiService = Communicator.getClient().create(TransaksiInterface.class);
        Call<ListCheckoutModel> call = apiService.getDetailCheckout(idUser);
        call.enqueue(new Callback<ListCheckoutModel>() {
            @Override
            public void onResponse(Call<ListCheckoutModel> call, Response<ListCheckoutModel> response) {
                if (response.isSuccessful()) {
                    listMenu = response.body();
                    if (!listMenu.isError()) {
                        idTransaksi = listMenu.getIdTransaksi();
                        isValidate = listMenu.getIsValidate();
                        idPemasukan = listMenu.getIdPemasukan();
                        adapter = new GroupByRestoRVAdapter(listMenu.getListWarung(), DetailCheckoutActivity.this);
                        groupByRestoRv.setAdapter(adapter);
                        showDialog(false, messageLoading);
                    }
                }
                showDialog(false, messageLoading);
            }

            @Override
            public void onFailure(Call<ListCheckoutModel> call, Throwable t) {
                // Log error here since request failed
                showDialog(false, messageLoading);
                myUtilities.showInfoDialog("Failed to request data", R.style.MyAlertDialogStyle, DetailCheckoutActivity.this, true);
            }
        });
//        adapter.notifyDataSetChanged();
    }

    @OnClick(R.id.btnCheckoutAlamat)
    public void onIsiAlamatClicked() {
        Intent mIntent = new Intent(this, AlamatUserActivity.class);
        startActivityForResult(mIntent, INTENT_ALAMAT_TRANSAKSI);
    }

    @OnClick(R.id.btnDetailCheckoutLanjut)
    public void onCheckoutLanjutClicked() {
        if (listOngkir.size() > 0)
            requestKurir();
        else
            myUtilities.showInfoDialog("Pilih Lokasi Anda", R.style.MyAlertDialogStyle, DetailCheckoutActivity.this, false);

    }

    @Override
    public void showBtnLanjut(boolean show) {
        if (show) btnLanjut.setVisibility(View.GONE);
        else btnLanjut.setVisibility(View.VISIBLE);
    }

    @Override
    public void showDialog(boolean show, String message) {
        progressdialog.setMessage(message);
        if (show) progressdialog.show();
        else progressdialog.dismiss();
    }

    /*@Override
    public void onClick(View view) {

        gak dipake dulu, soalnya untuk alamat diisi manual - no Google Maps
        if (view == btnPilihAlamat) {
            if (checker.lacksPermissions(PERMISSIONS_ACCESS_LOCATION)) {
                startPermissionsActivity(PERMISSIONS_ACCESS_LOCATION);
            } else {
                showDialog(true, messageLoading);
                try {
                    startActivityForResult(builder.build(DetailCheckoutActivity.this), PLACE_PICKER_REQUEST);
                    showDialog(false, "");
                } catch (GooglePlayServicesRepairableException e) {
                    showDialog(false, "");
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                    showDialog(false, "");
                }
            }
        }

        if (view == btnLanjut) {
            if (listOngkir.size() > 0)
                requestKurir();
            else
                myUtilities.showInfoDialog("Pilih Lokasi Anda", R.style.MyAlertDialogStyle, DetailCheckoutActivity.this, false);
        }
    }*/

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    private void getDistance(String origin, String destination) {
        showDialog(true, messageLoading);
        String urlMaps = "https://maps.googleapis.com/";
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(urlMaps)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MyMapService apiService = retrofit.create(MyMapService.class);
        Call<MapInformationModel> call = apiService.getMapInformation(origin, destination, MyStatic.API_MAP_KEY);
        call.enqueue(new Callback<MapInformationModel>() {
            @Override
            public void onResponse(Call<MapInformationModel> call, Response<MapInformationModel> response) {
                if (response.isSuccessful()) {
                    MapInformationModel data = response.body();
                    if (data != null) {
                        if (data.getStatus().equalsIgnoreCase("OK")) {
                            RowsModel rows = data.getRows()[0];
                            ElementsModel elements = rows.getElements()[0];
                            if (elements.getStatus().equalsIgnoreCase("OK")) {
                                DistanceModel distance = elements.getDistanceModel();
                                DurationModel duration = elements.getDurationModel();
                                if (distance != null && duration != null) {
                                    double jarak = Double.parseDouble(distance.getValue());
                                    Log.d("ongkir2", jarak + " m- " + (jarak / 1000) + " km" + myUtilities.getOngkir(jarak) + "");
                                    listOngkir.add(myUtilities.getOngkir(jarak));
                                    if (listOngkir.size() == 0) {

                                    } else if (listOngkir.size() == 1) {
                                        labelOngkir.setText(MyUtilities.setRupiah(myUtilities.getOngkir(jarak) + ""));
                                        btnLanjut.setVisibility(View.VISIBLE);
                                    } else if (listOngkir.size() == 2) {
                                        double totalOngkir = listOngkir.get(0) + myUtilities.getOngkir(jarak);
                                        labelOngkir.setText(MyUtilities.setRupiah(String.valueOf(totalOngkir)));
                                        btnLanjut.setVisibility(View.VISIBLE);
                                    }
                                    requestTotalHarga();

                                } else {
                                    myUtilities.showInfoDialog("Jarak dan waktu hampa", R.style.MyAlertDialogStyle, DetailCheckoutActivity.this, false);
                                    btnLanjut.setVisibility(View.GONE);
                                }
                            } else {
                                myUtilities.showInfoDialog("Whoop, please try again ", R.style.MyAlertDialogStyle, DetailCheckoutActivity.this, false);
                                btnLanjut.setVisibility(View.GONE);

                            }
                        } else {
                            myUtilities.showInfoDialog("Can't get response from server ....", R.style.MyAlertDialogStyle, DetailCheckoutActivity.this, false);
                            btnLanjut.setVisibility(View.GONE);
                        }
                    }
                }
                showDialog(false, messageLoading);
            }

            @Override
            public void onFailure(Call<MapInformationModel> call, Throwable t) {
                showDialog(false, messageLoading);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);

                labelAlamat.setText(place.getAddress());
                myLatitude = place.getLatLng().latitude;
                myLongitude = place.getLatLng().longitude;
                String origins = myLatitude + "," + myLongitude;
                if (!listMenu.isError()) {
                    listOngkir.clear();
                    listOngkir.add(10000.0);
                    labelOngkir.setText(MyUtilities.setRupiah(10000 + "")); /* ongkir flat Rp. 10.000*/
                    btnLanjut.setVisibility(View.VISIBLE);
                    requestTotalHarga();
//                    for (RestoModel model : listMenu.getListWarung()) {
//                        String destination = model.getLatitude() + "," + model.getLongitude();
//                        getDistance(origins, destination);
//                    }
                }
            }
        }

        if (requestCode == INTENT_ALAMAT_TRANSAKSI && resultCode == RESULT_OK) {
            userPemesanModel = (UserModel) data.getExtras().getSerializable("user_model");
            labelAlamat.setText(userPemesanModel.getAlamat());
            labelOngkir.setText(MyUtilities.setRupiah(10000 + "")); /* ongkir flat Rp. 10.000*/
            listOngkir.add(10000.0);
            requestTotalHarga();
        }

    }


    private void requestTotalHarga() {
        showDialog(true, messageLoading);
        TransaksiInterface api = Communicator.getClient().create(TransaksiInterface.class);
        Call<TransaksiModel> call = api.getTotalHarga(idUser);
        call.enqueue(new Callback<TransaksiModel>() {
            @Override
            public void onResponse(Call<TransaksiModel> call, Response<TransaksiModel> response) {
                if (response.isSuccessful()) {
                    double ongkir = 0;
                    for (double item : listOngkir) {
                        ongkir += item;
                    }

                    totalHarga = response.body().getTotalHarga();
                    totalTransaksi = (int) (response.body().getTotalHarga() + ongkir);
                    labelTotalHarga.setText(MyUtilities.setRupiah(response.body().getTotalHarga() + ""));
                    labelTotalBayar.setText(MyUtilities.setRupiah(totalTransaksi + ""));
                }
                showDialog(false, messageLoading);
            }

            @Override
            public void onFailure(Call<TransaksiModel> call, Throwable t) {
                showDialog(false, messageLoading);
                myUtilities.showInfoDialog(t.getMessage(), R.style.MyAlertDialogStyle, DetailCheckoutActivity.this, false);
            }
        });
    }

    private void requestKurir() {
        showDialog(true, "Mencari kurir....");
        KurirInterface api = Communicator.getClient().create(KurirInterface.class);

        // TODO auth key
        Call<KurirModel> call = api.getKurirTransaksi("");
        call.enqueue(new Callback<KurirModel>() {
            @Override
            public void onResponse(Call<KurirModel> call, Response<KurirModel> response) {
                showDialog(false, "");
                if (response.isSuccessful()) {
                    if (response.body().isError()) {
                        myUtilities.showInfoDialog(response.body().getMessage(), R.style.MyAlertDialogStyle, DetailCheckoutActivity.this, false);
                    } else {
                        Bundle args = new Bundle();
                        KurirModel kurirModel = response.body();
                        args.putSerializable("kurir_model", kurirModel);
                        args.putSerializable("user_pemesan_model", userPemesanModel);
                        args.putInt("id_kurir", kurirModel.getIdKurir());
                        args.putString("nama", kurirModel.getFirstName() + " " + kurirModel.getLastName());
                        args.putFloat("rate", kurirModel.getRate());
                        args.putString("phone", kurirModel.getPhone());
                        args.putString("foto", kurirModel.getFoto());
                        args.putInt("total_harga", totalHarga);
                        args.putInt("total_transaksi", totalTransaksi);
                        args.putString("latitude", myLatitude + "");
                        args.putString("longitude", myLongitude + "");
                        args.putInt("id_transaksi", idTransaksi);
                        args.putInt("is_validate", isValidate);
                        args.putInt("id_pemasukan", idPemasukan);

                        FragmentManager manager = getSupportFragmentManager();
                        KurirInfoDialog dialog = new KurirInfoDialog();
                        dialog.setArguments(args);
                        dialog.show(manager, "kurirDialog");
                    }
                } else
                    Toast.makeText(DetailCheckoutActivity.this, "not success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<KurirModel> call, Throwable t) {
                showDialog(false, "");
                myUtilities.showInfoDialog("Whoop, terjadi kesalahan", R.style.MyAlertDialogStyle, DetailCheckoutActivity.this, false);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    private void startPermissionsActivity(String[] permission) {
        PermissionsActivity.startActivityForResult(this, 0, permission);
    }
}
