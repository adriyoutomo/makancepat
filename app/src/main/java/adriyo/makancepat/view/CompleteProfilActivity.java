package adriyo.makancepat.view;

import android.support.v4.app.NavUtils;
import android.support.v4.text.TextUtilsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import adriyo.makancepat.R;

public class CompleteProfilActivity extends AppCompatActivity {

    Button btnSelesai;
    EditText txtNoKTP, txtAlamat, txtBank, txtNoRekening, txtNoTelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_profil);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (getSupportActionBar() == null) {
            Log.d("actionbar", "gak kosong");
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Data Lengkap");
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavUtils.navigateUpFromSameTask(CompleteProfilActivity.this);
                }
            });

            // Inflate a menu to be displayed in the toolbar
            //actionbar.inflateMenu(R.menu.settings);
        }
        init();
    }

    private void init() {
        txtAlamat = (EditText) findViewById(R.id.txtCompleteAlamat);
        txtBank = (EditText) findViewById(R.id.txtCompleteBank);
        txtNoKTP = (EditText) findViewById(R.id.txtCompleteNoKTP);
        txtNoRekening = (EditText) findViewById(R.id.txtCompleteNoRekening);
        txtNoTelp = (EditText) findViewById(R.id.txtCompleteNoTelp);

        btnSelesai = (Button) findViewById(R.id.btnCompleteSelesai);
        btnSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLogin();
            }
        });
    }

    private void checkLogin() {
        String alamat = txtAlamat.getText().toString().trim();
        String bank = txtBank.getText().toString().trim();
        String noKTP = txtNoKTP.getText().toString().trim();
        String noRekening = txtNoRekening.getText().toString().trim();
        String noTelp = txtNoTelp.getText().toString().trim();

        txtNoTelp.setError(null);
        txtNoRekening.setError(null);
        txtNoKTP.setError(null);
        txtBank.setError(null);
        txtAlamat.setError(null);

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(noKTP)) {
            txtNoKTP.setError("No. KTP kosong");
            focusView = txtNoKTP;
            cancel = true;
        }else if (TextUtils.isEmpty(alamat)) {
            txtAlamat.setError("Alamat kosong");
            focusView = txtAlamat;
            cancel = true;
        } else if (TextUtils.isEmpty(bank)) {
            txtBank.setError("Bank kosong");
            focusView = txtBank;
            cancel = true;
        } else if (TextUtils.isEmpty(noRekening)) {
            txtNoRekening.setError("No. Rekening kosong");
            focusView = txtNoRekening;
            cancel = true;
        } else if (TextUtils.isEmpty(noTelp)) {
            txtNoTelp.setError("No. Telp kosong");
            focusView = txtNoTelp;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else finish();
    }

}
