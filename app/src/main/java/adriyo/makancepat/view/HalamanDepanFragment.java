package adriyo.makancepat.view;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.BannerSliderAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class HalamanDepanFragment extends Fragment {

    Button btnOrder;
    HomeFragmentListener listener;
    PopupWindow popupWindow;
    LinearLayout layoutPopUp;

    private ViewPager viewPager;
    private BannerSliderAdapter sliderAdapter;
    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;

    public HalamanDepanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (HomeFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement HomeFragmentListener");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.post(new Runnable() {
            @Override
            public void run() {
//                initPopUp(view);
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_halaman_depan, container, false);
        btnOrder = (Button) rootView.findViewById(R.id.homeBtnOrderSekarang);
        ((HomeActivity) getActivity()).setMyTitleBar("Halaman Utama");
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = 1;
                listener.setSelectedDrawer(i);
            }
        });
        pager_indicator = (LinearLayout) rootView.findViewById(R.id.bannerIndicator);

        viewPager = (ViewPager) rootView.findViewById(R.id.vpBanner);
        sliderAdapter = new BannerSliderAdapter(getActivity());
        viewPager.setAdapter(sliderAdapter);
        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotsCount; i++) {
//                    if (i!=position)
                    dots[i].setSelected(false);
                }
                dots[position].setSelected(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setupSlider();

        return rootView;
    }

    private void setupSlider() {
        dotsCount = sliderAdapter.getCount();
        dots = new ImageView[dotsCount];
        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getActivity());
            dots[i].setImageResource(R.drawable.indicator_circle);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(4, 0, 4, 0);
            pager_indicator.addView(dots[i], params);
        }
        dots[0].setSelected(true);


    }

    public interface HomeFragmentListener {
        public void setSelectedDrawer(int i);
    }

}
