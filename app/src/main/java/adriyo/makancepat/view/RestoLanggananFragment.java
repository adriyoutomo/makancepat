package adriyo.makancepat.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import adriyo.makancepat.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RestoLanggananFragment extends Fragment {


    public RestoLanggananFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_resto_langganan, container, false);
    }

}
