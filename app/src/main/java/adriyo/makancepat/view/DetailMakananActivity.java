package adriyo.makancepat.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.ImageMakananAdapter;
import adriyo.makancepat.customutil.MyStatic;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.DaftarPesananResponse;
import adriyo.makancepat.model.GaleriMakananResponse;
import adriyo.makancepat.model.ImageMakananModel;
import adriyo.makancepat.model.MenuMakananModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.DaftarPesananInterface;
import adriyo.makancepat.rest.GaleriMakananInterface;
import adriyo.makancepat.rest.MenuMakananInterface;
import adriyo.makancepat.ui.transaksi.CheckoutActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetailMakananActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnLanjut;
    TextView labelNamaMakanan, labelHarga, labelNamaWarung, labelDeskripsi, labelHari, labelJam, labelAlamat;
    List<MenuMakananModel> listMenu;
    SessionManager sessionManager;
    MenuMakananModel model;
    String id;
    ViewPager viewPager;
    private ImageMakananAdapter adapterView;
    private List<ImageMakananModel> dataImage;
    int qty = 0;
    TextView txtQty;
    MyUtilities myUtilities;
    ImageView logoResto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_makanan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (getSupportActionBar() == null) {
            Log.d("actionbar", "gak kosong");
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(R.string.title_activity_detail_makanan);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //NavUtils.navigateUpFromSameTask(DetailMakananActivity.this);
                    finish();
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

                }
            });
        }
        init();

        Bundle extras = getIntent().getExtras();
        id = extras.getString("idMakanan");
        loadData(Integer.parseInt(id));
        requestDataImage(Integer.parseInt(id));


    }

    private void loadData(int id) {
        MenuMakananInterface apiService = Communicator.getClient().create(MenuMakananInterface.class);
        Call<MenuMakananModel> call = apiService.getMenuMakananDetail(id);
        call.enqueue(new Callback<MenuMakananModel>() {
            @Override
            public void onResponse(Call<MenuMakananModel> call, Response<MenuMakananModel> response) {
                model = response.body();
                labelNamaMakanan.setText(Html.fromHtml("<h1>" + model.getNamaMenu() + "</h1>"));
                labelHarga.setText(Html.fromHtml("<h3>Rp. " + model.getHarga() + "</h2>"));
                labelNamaWarung.setText(Html.fromHtml("<b>" + model.getNamaWarung() + "</b>"));
                labelDeskripsi.setText(model.getDeskripsiWarung());
                labelHari.setText(model.getHari());
                labelJam.setText(model.getJam());
                labelAlamat.setText(model.getAlamat());
                String url = Communicator.BASE_URL + model.getLogoWarung();
                Picasso.with(DetailMakananActivity.this)
                        .load(url).placeholder(R.drawable.logo_makancepat).into(logoResto);

            }

            @Override
            public void onFailure(Call<MenuMakananModel> call, Throwable t) {
                // Log error here since request failed
                Toast.makeText(getApplicationContext(), "Failed to request data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void init() {

        labelDeskripsi = (TextView) findViewById(R.id.txtDeskripsi);
        labelHarga = (TextView) findViewById(R.id.labelDetailPesanHargaMakanan);
        labelNamaMakanan = (TextView) findViewById(R.id.labelDetailPesanNamaMakanan);
        labelNamaWarung = (TextView) findViewById(R.id.txtNamaWarung);
        labelHari = (TextView) findViewById(R.id.txtHari);
        labelJam = (TextView) findViewById(R.id.txtJam);
        labelAlamat = (TextView) findViewById(R.id.txtAlamat);

        logoResto = (ImageView) findViewById(R.id.logoResto);

        btnLanjut = (Button) findViewById(R.id.btnDetailPesanLanjut);
        btnLanjut.setOnClickListener(this);

        sessionManager = new SessionManager(getApplicationContext());

        viewPager = (ViewPager) findViewById(R.id.viewPagerMenuMakanan);
        myUtilities = new MyUtilities();
    }

    @Override
    public void onClick(View view) {
        if (view == btnLanjut) {
            showChangeLangDialog();
            //startActivity(new Intent(DetailMakananActivity.this, CheckoutActivity.class));
        }
    }


    public void showChangeLangDialog() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_pesan_menu, null);
        dialogBuilder.setView(dialogView);
        txtQty = (TextView) dialogView.findViewById(R.id.quantity_text_view);
        display(qty);
        final EditText txtCatatan = (EditText) dialogView.findViewById(R.id.txtDetailCatatan);
        dialogBuilder.setTitle("Keterangan Pemesanan");
        dialogBuilder.setMessage("Isi jumlah pemesanan dan catatan (jika ada)");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.show();

        b.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int idUser = Integer.parseInt(sessionManager.getUserDetails().get(SessionManager.KEY_ID_USER));
                int idMenu = Integer.parseInt(model.getIdMenu());
                String note = txtCatatan.getText().toString().trim();
                if (qty > 0)
                    postData(idUser, idMenu, qty, note);
                else
                    Toast.makeText(DetailMakananActivity.this, "Isi jumlah pesanan", Toast.LENGTH_SHORT).show();
                b.dismiss();
            }
        });

        b.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                b.dismiss();
            }
        });
    }

    public void postData(int idUser, int idMenu, int qty, String note) {
        DaftarPesananInterface apiService = Communicator.getClient().create(DaftarPesananInterface.class);
        Call<DaftarPesananResponse> call = apiService.insertTransaksi(idUser, idMenu, qty, note);
        call.enqueue(new Callback<DaftarPesananResponse>() {
            @Override
            public void onResponse(Call<DaftarPesananResponse> call, Response<DaftarPesananResponse> response) {
                if (response.isSuccessful()) {
                    if (!response.body().isError()) {
                        finish();
                        overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
                        startActivity(new Intent(DetailMakananActivity.this, CheckoutActivity.class));
                    } else {
                        myUtilities.showInfoDialog(
                                response.body().getMessage(),
                                R.style.MyAlertDialogStyle,
                                DetailMakananActivity.this, true);
                    }
                }else{
                    Toast.makeText(DetailMakananActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DaftarPesananResponse> call, Throwable t) {
                // Log error here since request failed
                Toast.makeText(getApplicationContext(), "Failed to request data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void requestDataImage(int idMenu) {
        GaleriMakananInterface apiService = Communicator.getClient().create(GaleriMakananInterface.class);
        Call<GaleriMakananResponse> call = apiService.getGaleriMakanan(idMenu);
        call.enqueue(new Callback<GaleriMakananResponse>() {
            @Override
            public void onResponse(Call<GaleriMakananResponse> call, Response<GaleriMakananResponse> response) {
                if (response.isSuccessful()) {
                    dataImage = response.body().getList();
                    Log.d("sizeDataImage", "" + dataImage.size());
                    if (dataImage.size() > 0) {
                        adapterView = new ImageMakananAdapter(DetailMakananActivity.this, dataImage);
                        viewPager.setAdapter(adapterView);
                    } else {
                        ImageMakananModel item = new ImageMakananModel();
                        item.setUrl(MyStatic.urlImageNotAvailable);
                        dataImage.add(item);
                        adapterView = new ImageMakananAdapter(DetailMakananActivity.this, dataImage);
                        viewPager.setAdapter(adapterView);
                    }
                }
            }

            @Override
            public void onFailure(Call<GaleriMakananResponse> call, Throwable t) {
                // Log error here since request failed
                //Toast.makeText(DetailMakananActivity.this, "Failed to request data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void display(int i) {
        txtQty.setText("" + i);
    }


    public void decrement(View view) {
        if (qty > 0)
            qty = qty - 1;
        display(qty);

    }

    public void increment(View view) {
        qty = qty + 1;
        display(qty);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
}
