package adriyo.makancepat;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import adriyo.makancepat.view.MainActivity;

/**
 * Created by adriyo on 3/16/2017.
 */

public class BaseActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        toolbar = getToolbar();
    }

    private Toolbar getToolbar() {
        if (toolbar == null) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setNavigationContentDescription("Toolbar");
            setSupportActionBar(toolbar);
        }
        return toolbar;
    }

    /**
     *
     * @param openMainActivity set true when start activity is occuring
     * @param context current context
     */
    public void setToolbarAsUp(boolean openMainActivity, Context context) {
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            toolbar.setNavigationOnClickListener(v -> {
                if (openMainActivity) {
                    startActivity(new Intent(context, MainActivity.class));
                    finish();
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                } else {
                    finish();
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }
            });
        }
    }

    public void setToolbarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        } else toolbar = getToolbar();
    }

}
