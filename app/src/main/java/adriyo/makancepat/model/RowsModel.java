package adriyo.makancepat.model;

/**
 * Created by ADMIN on 1/15/2017.
 */

public class RowsModel {
    private ElementsModel[] elements;

    public ElementsModel[] getElements() {
        return elements;
    }

    public void setElements(ElementsModel[] elements) {
        this.elements = elements;
    }

    @Override
    public String toString() {
        return "ClassPojo [elements = " + elements + "]";
    }
}
