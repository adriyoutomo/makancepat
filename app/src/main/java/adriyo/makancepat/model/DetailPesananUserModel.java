package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 11/14/2016.
 */

public class DetailPesananUserModel {
    @SerializedName("id")
    int id;

    @SerializedName("id_menu")
    int idMenu;

    @SerializedName("nama_menu")
    String namaMenu;

    @SerializedName("harga")
    int harga;

    @SerializedName("qty")
    int qty;

    @SerializedName("note")
    String note;

    @SerializedName("id_warung")
    int idWarung;

    @SerializedName("nama_warung")
    String namaWarung;

    @SerializedName("is_ship")
    int isShip;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(int idMenu) {
        this.idMenu = idMenu;
    }

    public String getNamaMenu() {
        return namaMenu;
    }

    public void setNamaMenu(String namaMenu) {
        this.namaMenu = namaMenu;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getIdWarung() {
        return idWarung;
    }

    public void setIdWarung(int idWarung) {
        this.idWarung = idWarung;
    }

    public String getNamaWarung() {
        return namaWarung;
    }

    public void setNamaWarung(String namaWarung) {
        this.namaWarung = namaWarung;
    }

    public int getIsShip() {
        return isShip;
    }

    public void setIsShip(int isShip) {
        this.isShip = isShip;
    }
}
