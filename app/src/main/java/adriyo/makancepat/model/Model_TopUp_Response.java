package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by USER on 11/14/2016.
 */

public class Model_TopUp_Response {
    @SerializedName("error")
    private boolean error;

    @SerializedName("message")
    private String message;

    @SerializedName("detail")
    private List<Model_TopUp> listTopUp;

    public Model_TopUp_Response(boolean error, List<Model_TopUp> listMenu) {
        this.error = error;
        this.listTopUp = listMenu;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<Model_TopUp> getListTopUp() {
        return listTopUp;
    }

    public void setListTopUp(List<Model_TopUp> listTopUp) {
        this.listTopUp = listTopUp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
