package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 11/20/2016.
 */

public class ImageMakananModel {
    @SerializedName("id_galeri_menu")
    private int idGaleriMenu;

    @SerializedName("id_menu")
    private int idMenu;

    @SerializedName("filepath")
    private String filepath;

    @SerializedName("filename")
    private String filename;

    @SerializedName("url")
    private String url;

    public int getIdGaleriMenu() {
        return idGaleriMenu;
    }

    public void setIdGaleriMenu(int idGaleriMenu) {
        this.idGaleriMenu = idGaleriMenu;
    }

    public int getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(int idMenu) {
        this.idMenu = idMenu;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
