package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 11/20/2016.
 */

public class ImageWarungModel {
    @SerializedName("id")
    private int id;

    @SerializedName("id_warung")
    private int idWarung;

    @SerializedName("filepath")
    private String filepath;

    @SerializedName("filename")
    private String filename;

    @SerializedName("url")
    private String url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdWarung() {
        return idWarung;
    }

    public void setIdWarung(int idWarung) {
        this.idWarung = idWarung;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
