package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 12/1/2016.
 */

public class MenuRestoModel {


    @SerializedName("id_menu")
    private int idMenu;

    @SerializedName("nama_warung")
    private String namaWarung;

    @SerializedName("nama_menu")
    private String namaMenu;

    @SerializedName("harga")
    private String harga;

    @SerializedName("url")
    private String url;

    public int getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(int idMenu) {
        this.idMenu = idMenu;
    }

    public String getNamaWarung() {
        return namaWarung;
    }

    public void setNamaWarung(String namaWarung) {
        this.namaWarung = namaWarung;
    }

    public String getNamaMenu() {
        return namaMenu;
    }

    public void setNamaMenu(String namaMenu) {
        this.namaMenu = namaMenu;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
