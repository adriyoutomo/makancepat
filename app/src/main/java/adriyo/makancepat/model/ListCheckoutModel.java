package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ADMIN on 1/20/2017.
 */

public class ListCheckoutModel {

    @SerializedName("error")
    private boolean error;

    @SerializedName("id_transaksi")
    private int idTransaksi;

    @SerializedName("is_validate")
    private int isValidate;

    @SerializedName("id_pemasukan")
    private int idPemasukan;

    @SerializedName("warung_makan")
    private List<RestoModel> listWarung;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<RestoModel> getListWarung() {
        return listWarung;
    }

    public void setListWarung(List<RestoModel> listWarung) {
        this.listWarung = listWarung;
    }

    public int getIdTransaksi() {
        return idTransaksi;
    }

    public void setIdTransaksi(int idTransaksi) {
        this.idTransaksi = idTransaksi;
    }

    public int getIsValidate() {
        return isValidate;
    }

    public void setIsValidate(int isValidate) {
        this.isValidate = isValidate;
    }

    public int getIdPemasukan() {
        return idPemasukan;
    }

    public void setIdPemasukan(int idPemasukan) {
        this.idPemasukan = idPemasukan;
    }
}
