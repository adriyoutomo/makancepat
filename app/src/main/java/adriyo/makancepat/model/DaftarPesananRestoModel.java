package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ADMIN on 11/14/2016.
 */

public class DaftarPesananRestoModel {

    @SerializedName("id_transaksi")
    private int idTransaksi;

    @SerializedName("id_kurir")
    private int idKurir;

    @SerializedName("id_user")
    private int idUser;

    @SerializedName("total_transaksi")
    private int totalTransaksi;

    @SerializedName("waktu_transaksi")
    private String waktuTransaksi;

    @SerializedName("note")
    private String note;

    @SerializedName("detail_pesanan")
    private List<DetailPesananRestoModel> detailPesanan;

    public int getIdTransaksi() {
        return idTransaksi;
    }

    public void setIdTransaksi(int idTransaksi) {
        this.idTransaksi = idTransaksi;
    }

    public int getIdKurir() {
        return idKurir;
    }

    public void setIdKurir(int idKurir) {
        this.idKurir = idKurir;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getTotalTransaksi() {
        return totalTransaksi;
    }

    public void setTotalTransaksi(int totalTransaksi) {
        this.totalTransaksi = totalTransaksi;
    }

    public String getWaktuTransaksi() {
        return waktuTransaksi;
    }

    public void setWaktuTransaksi(String waktuTransaksi) {
        this.waktuTransaksi = waktuTransaksi;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<DetailPesananRestoModel> getDetailPesanan() {
        return detailPesanan;
    }

    public void setDetailPesanan(List<DetailPesananRestoModel> detailPesanan) {
        this.detailPesanan = detailPesanan;
    }
}
