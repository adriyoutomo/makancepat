package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ADMIN on 11/30/2016.
 */

public class SearchResultResponse {

    @SerializedName("error")
    private boolean error;

    @SerializedName("detail")
    private List<MenuMakananModel> list;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<MenuMakananModel> getList() {
        return list;
    }

    public void setList(List<MenuMakananModel> list) {
        this.list = list;
    }
}
