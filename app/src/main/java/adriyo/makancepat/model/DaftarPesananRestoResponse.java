package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ADMIN on 11/14/2016.
 */

public class DaftarPesananRestoResponse {

    @SerializedName("error")
    private boolean error;

    @SerializedName("pesanan")
    private List<DaftarPesananModel> listMenu;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<DaftarPesananModel> getListMenu() {
        return listMenu;
    }

    public void setListMenu(List<DaftarPesananModel> listMenu) {
        this.listMenu = listMenu;
    }
}
