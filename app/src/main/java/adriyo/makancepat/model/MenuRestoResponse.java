package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 12/1/2016.
 */

public class MenuRestoResponse {

    @SerializedName("error")
    private boolean error;

    @SerializedName("galeri")
    private List<MenuRestoModel> list;

    public MenuRestoResponse(boolean error, List<MenuRestoModel> list) {
        this.error = error;
        this.list = list;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<MenuRestoModel> getList() {
        return list;
    }

    public void setList(List<MenuRestoModel> list) {
        this.list = list;
    }
}
