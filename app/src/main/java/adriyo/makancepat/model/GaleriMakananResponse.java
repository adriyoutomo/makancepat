package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ADMIN on 11/20/2016.
 */

public class GaleriMakananResponse {

    @SerializedName("error")
    private boolean error;

    @SerializedName("galeri")
    private List<ImageMakananModel> list;

    public GaleriMakananResponse(boolean error, List<ImageMakananModel> list) {
        this.error = error;
        this.list = list;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<ImageMakananModel> getList() {
        return list;
    }

    public void setList(List<ImageMakananModel> list) {
        this.list = list;
    }
}
