package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ADMIN on 11/14/2016.
 */

public class DetailPesananUserResponse {

    @SerializedName("error")
    private boolean error;

    @SerializedName("pesanan")
    private List<DetailPesananUserModel> listMenu;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<DetailPesananUserModel> getListMenu() {
        return listMenu;
    }

    public void setListMenu(List<DetailPesananUserModel> listMenu) {
        this.listMenu = listMenu;
    }
}
