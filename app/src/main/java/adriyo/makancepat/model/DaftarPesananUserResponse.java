package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ADMIN on 11/14/2016.
 */

public class DaftarPesananUserResponse {

    @SerializedName("error")
    private boolean error;

    @SerializedName("pesanan")
    private List<DaftarPesananUserModel> listMenu;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<DaftarPesananUserModel> getListMenu() {
        return listMenu;
    }

    public void setListMenu(List<DaftarPesananUserModel> listMenu) {
        this.listMenu = listMenu;
    }
}
