package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 11/1/2016.
 */

public class MenuMakananModel {
    @SerializedName("error")
    boolean error;

    @SerializedName("id_menu")
    String idMenu;

    @SerializedName("id_warung")
    String idWarung;

    @SerializedName("nama_menu")
    String namaMenu;

    @SerializedName("nama_warung")
    String namaWarung;

    @SerializedName("alamat")
    String alamat;

    @SerializedName("harga")
    String harga;

    @SerializedName("url")
    private String url;

    @SerializedName("qty")
    private int qty;

    @SerializedName("deskripsi")
    private String deskripsiWarung;

    @SerializedName("hari")
    private String hari;

    @SerializedName("jam")
    private String jam;

    @SerializedName("logo_warung")
    private String logoWarung;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(String idMenu) {
        this.idMenu = idMenu;
    }

    public String getIdWarung() {
        return idWarung;
    }

    public void setIdWarung(String idWarung) {
        this.idWarung = idWarung;
    }

    public String getNamaMenu() {
        return namaMenu;
    }

    public void setNamaMenu(String namaMenu) {
        this.namaMenu = namaMenu;
    }

    public String getNamaWarung() {
        return namaWarung;
    }

    public void setNamaWarung(String namaWarung) {
        this.namaWarung = namaWarung;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getDeskripsiWarung() {
        return deskripsiWarung;
    }

    public void setDeskripsiWarung(String deskripsiWarung) {
        this.deskripsiWarung = deskripsiWarung;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getLogoWarung() {
        return logoWarung;
    }

    public void setLogoWarung(String logoWarung) {
        this.logoWarung = logoWarung;
    }
}
