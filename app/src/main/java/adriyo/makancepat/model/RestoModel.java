package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


/**
 * Created by ADMIN on 11/3/2016.
 */

public class RestoModel {
    @SerializedName("error")
    boolean error;

    @SerializedName("id_warung")
    String idWarung;

    @SerializedName("nama_warung")
    String namaWarung;

    @SerializedName("alamat")
    String alamat;

    @SerializedName("hari")
    String hari;

    @SerializedName("jam")
    String jam;

    @SerializedName("deskripsi")
    String deskripsi;

    @SerializedName("email")
    String email;

    @SerializedName("latitude")
    String latitude;

    @SerializedName("longitude")
    String longitude;

    @SerializedName("message")
    String message;

    @SerializedName("menu_makanan")
    List<MenuMakananModel> listMakanan;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getIdWarung() {
        return idWarung;
    }

    public void setIdWarung(String idWarung) {
        this.idWarung = idWarung;
    }

    public String getNamaWarung() {
        return namaWarung;
    }

    public void setNamaWarung(String namaWarung) {
        this.namaWarung = namaWarung;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MenuMakananModel> getListMakanan() {
        return listMakanan;
    }

    public void setListMakanan(List<MenuMakananModel> listMakanan) {
        this.listMakanan = listMakanan;
    }
}
