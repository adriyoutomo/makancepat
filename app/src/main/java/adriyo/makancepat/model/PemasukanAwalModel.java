package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 2/25/2017.
 */

public class PemasukanAwalModel {

    @SerializedName("id_pemasukan")
    int idPemasukan;

    @SerializedName("id_user")
    int idUser;

    @SerializedName("id_saldo")
    int idSaldo;

    @SerializedName("besaran")
    int besaran;

    @SerializedName("created_at")
    String createdAt;

    @SerializedName("is_paid")
    int isPaid;

    @SerializedName("images")
    String images;

    @SerializedName("jenis_transaksi")
    int jenisTransaksi;

    public int getIdPemasukan() {
        return idPemasukan;
    }

    public void setIdPemasukan(int idPemasukan) {
        this.idPemasukan = idPemasukan;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdSaldo() {
        return idSaldo;
    }

    public void setIdSaldo(int idSaldo) {
        this.idSaldo = idSaldo;
    }

    public int getBesaran() {
        return besaran;
    }

    public void setBesaran(int besaran) {
        this.besaran = besaran;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(int isPaid) {
        this.isPaid = isPaid;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public int getJenisTransaksi() {
        return jenisTransaksi;
    }

    public void setJenisTransaksi(int jenisTransaksi) {
        this.jenisTransaksi = jenisTransaksi;
    }
}
