package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ADMIN on 11/14/2016.
 */

public class DaftarPesananResponse {

    @SerializedName("error")
    private boolean error;

    @SerializedName("detail_transaksi")
    private List<DaftarPesananModel> listMenu;

    @SerializedName("message")
    private String message;

    public DaftarPesananResponse(boolean error, List<DaftarPesananModel> listMenu, String message) {
        this.error = error;
        this.listMenu = listMenu;
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<DaftarPesananModel> getListMenu() {
        return listMenu;
    }

    public void setListMenu(List<DaftarPesananModel> listMenu) {
        this.listMenu = listMenu;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
