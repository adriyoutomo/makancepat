package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ADMIN on 11/3/2016.
 */

public class RestoResponse {
    @SerializedName("error")
    private boolean error;

    @SerializedName("warung_makan")
    private List<RestoModel> listResto;

    public RestoResponse(boolean error, List<RestoModel> listResto) {
        this.error = error;
        this.listResto = listResto;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<RestoModel> getListResto() {
        return listResto;
    }

    public void setListResto(List<RestoModel> listResto) {
        this.listResto = listResto;
    }
}
