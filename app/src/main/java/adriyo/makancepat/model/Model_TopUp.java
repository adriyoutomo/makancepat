package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by USER on 11/10/2016.
 */

public class Model_TopUp {
    @SerializedName("error")
    boolean error;
    @SerializedName("id_pemasukan")
    private int id;
    @SerializedName("username")
    private String username;
    @SerializedName("besaran")
    private float saldo_awal;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("is_paid")
    private int is_paid;
    @SerializedName("images")
    private String images;
    @SerializedName("message")
    private String message;

    public Model_TopUp(int id, String username, float saldo_awal, String created_at, int is_paid, String images) {
        this.id = id;
        this.username = username;
        this.saldo_awal = saldo_awal;
        this.created_at = created_at;
        this.is_paid = is_paid;
        this.images = images;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public float getSaldo_awal() {
        return saldo_awal;
    }

    public void setSaldo_awal(float saldo_awal) {
        this.saldo_awal = saldo_awal;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getIs_paid() {
        return is_paid;
    }

    public void setIs_paid(int is_paid) {
        this.is_paid = is_paid;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
