package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 1/13/2017.
 */

public class InfoModel {
    @SerializedName("error")
    boolean error;

    @SerializedName("message")
    String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
