package adriyo.makancepat.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 11/14/2016.
 */

public class DetailPesananRestoModel {
    @SerializedName("id")
    int id;

    @SerializedName("id_menu")
    int idMenu;

    @SerializedName("nama_menu")
    String namaMenu;

    @SerializedName("harga")
    int harga;

    @SerializedName("qty")
    int qty;

    @SerializedName("note")
    String note;

}
