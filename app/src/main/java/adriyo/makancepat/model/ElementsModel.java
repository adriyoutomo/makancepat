package adriyo.makancepat.model;


import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 1/15/2017.
 */

public class ElementsModel {
    @SerializedName("duration")
    private DurationModel DurationModel;

    @SerializedName("distance")
    private DistanceModel DistanceModel;

    @SerializedName("status")
    private String status;

    public DurationModel getDurationModel() {
        return DurationModel;
    }

    public void setDurationModel(DurationModel DurationModel) {
        this.DurationModel = DurationModel;
    }

    public DistanceModel getDistanceModel() {
        return DistanceModel;
    }

    public void setDistanceModel(DistanceModel DistanceModel) {
        this.DistanceModel = DistanceModel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClassPojo [DurationModel = " + DurationModel + ", DistanceModel = " + DistanceModel + ", status = " + status + "]";
    }
}
