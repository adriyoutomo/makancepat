package adriyo.makancepat.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import adriyo.makancepat.MyApplication;
import adriyo.makancepat.R;
import adriyo.makancepat.customutil.NetworkHelper;

/**
 * Created by adriyo on 3/8/2017.
 */

public class ConnectivityReceiver extends BroadcastReceiver {

    private static final String TAG = "ConnectivityReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive" + NetworkHelper.isConnectedToInternet(context));
        if(NetworkHelper.isConnectedToInternet(context)){
            Toast.makeText(context, R.string.error_network, Toast.LENGTH_SHORT).show();
        }
    }
}
