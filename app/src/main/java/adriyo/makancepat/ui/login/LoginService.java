package adriyo.makancepat.ui.login;

import adriyo.makancepat.model.UserModel;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by adriyo on 3/23/2017.
 */

public interface LoginService {
    @FormUrlEncoded
    @POST("v1/login")
    Observable<UserModel> checkLogin(@Field("email") String email, @Field("password") String password);
}
