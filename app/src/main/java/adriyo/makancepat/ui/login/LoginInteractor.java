package adriyo.makancepat.ui.login;

import android.text.TextUtils;

import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.UserModel;

/**
 * Created by adriyo on 3/23/2017.
 */

class LoginInteractor {

    private LoginService service;

    public boolean validateLogin(String email, String password) {
        return email.equals("admin") && password.equals("asu");
    }

    public boolean isValidEmail(String email){
        if (TextUtils.isEmpty(email)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public void createLoginSession(SessionManager sessionManager, UserModel model) {
        sessionManager.createLoginSession(model);
    }
}
