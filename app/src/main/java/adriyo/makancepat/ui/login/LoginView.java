package adriyo.makancepat.ui.login;

import android.support.annotation.StringRes;

/**
 * Created by adriyo on 3/23/2017.
 */

public interface LoginView {

    void showErrorEmptyEmail(@StringRes int resId);
    void showErrorEmptyPassword(@StringRes int error_invalid_password);
    String getEmail();
    String getPassword();
    void showToastFailedLogin();
    void startMainActivity();
    void showProgress(final boolean val);

    void setErrorMessage(String message);
    void setErrorMessage(@StringRes int resId);

    void startManagementActivity();
}
