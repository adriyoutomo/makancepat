package adriyo.makancepat.ui.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import adriyo.makancepat.BaseActivity;
import adriyo.makancepat.R;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.UserModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.PelangganInterface;
import adriyo.makancepat.view.HomeActivity;
import adriyo.makancepat.view.MainActivity;
import adriyo.makancepat.view.ManagementActivity;
import adriyo.makancepat.view.ResetPasswordActivity;
import adriyo.makancepat.view.VerifikasiEmailActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseActivity implements LoginView {

    // UI references.
    @BindView(R.id.txt_email)
    AutoCompleteTextView mEmailView;
    @BindView(R.id.txt_password)
    EditText mPasswordView;
    @BindView(R.id.login_progress)
    View mProgressView;
    @BindView(R.id.login_form)
    View mLoginFormView;
    @BindView(R.id.labelErrorLogin)
    TextView txtErrLogin;
    @BindView(R.id.btn_sign_in)
    Button mEmailSignInButton;



    private SessionManager sessionManager;
    private LoginPresenter loginPresenter;
    private PelangganInterface apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
//        setupActionBar();
        setToolbarTitle(getResources().getString(R.string.title_activity_login));
        setToolbarAsUp(true, this);

        apiService = Communicator.getClient().create(PelangganInterface.class);
        sessionManager = new SessionManager(getApplicationContext());
        loginPresenter = new LoginPresenter(this, new LoginInteractor(), apiService, sessionManager);
        //mPasswordView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
//                if (id == R.id.login || id == EditorInfo.IME_NULL) {
//                    //attemptLogin();
//                    return true;
//                }
//                return false;
//            }
//        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        txtErrLogin = (TextView) findViewById(R.id.labelErrorLogin);
    }

    private void setupLogin() {
        showProgress(true);
        String email = mEmailView.getText().toString();
        String pass = mPasswordView.getText().toString();
        PelangganInterface apiService = Communicator.getClient().create(PelangganInterface.class);
        Call<UserModel> user = apiService.checkLogin(email, pass);
        user.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                showProgress(false);
                UserModel model = response.body();
                if (model != null) {
                    if (model.isError())
                        txtErrLogin.setText(model.getMessage());
                    else {
                        if (model.getJabatan().equals("admin")) {
                            sessionManager.createLoginSession(model);
                            Intent intent = new Intent(LoginActivity.this, ManagementActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            sessionManager.createLoginSession(model);
                            Log.d("LoginActivity", model.getFoto());
                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                } else {
                    txtErrLogin.setText("Ooops, something happened...");
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                showProgress(false);
                txtErrLogin.setText("Failed to connect the server");
            }
        });
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            setupLogin();

        }
    }

    @OnClick(R.id.btn_sign_in)
    public void btnSignInOnClicked(){
//        loginPresenter.onLoginButtonClick();
        loginPresenter.onCheckButtonClick();
    }

    private boolean isEmailValid(String email) {

        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {

        return password.length() >= 6;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void setErrorMessage(@StringRes int resId) {

    }

    @Override
    public void setErrorMessage(String message) {
        txtErrLogin.setText(message);
    }

    public void labelVerifikasiEmail(View view) {
        Intent intent = new Intent(LoginActivity.this, VerifikasiEmailActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    public void labelResetPassword(View view) {
        Intent intent = new Intent(LoginActivity.this, ResetPasswordActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        super.onBackPressed();
    }

    @Override
    public void showErrorEmptyEmail(@StringRes int resId) {
        mEmailView.setError(getString(resId));
    }

    @Override
    public void showErrorEmptyPassword(@StringRes int resId) {
        mPasswordView.setError(getString(resId));
    }

    @Override
    public String getEmail() {
        return mEmailView.getText().toString().trim();
    }

    @Override
    public String getPassword() {
        return mPasswordView.getText().toString().trim();
    }

    @Override
    public void showToastFailedLogin() {
        Toast.makeText(this, R.string.error_failed_login, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startMainActivity() {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void startManagementActivity() {
//        sessionManager.createLoginSession(model);
        Intent intent = new Intent(LoginActivity.this, ManagementActivity.class);
        startActivity(intent);
        finish();
    }
}

