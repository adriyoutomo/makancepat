package adriyo.makancepat.ui.login;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.UserModel;
import adriyo.makancepat.rest.PelangganInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by adriyo on 3/23/2017.
 */

class LoginPresenter {

    private final LoginView view;
    private final LoginInteractor loginInteractor;
    private PelangganInterface apiService;
    private SessionManager sessionManager;

    public LoginPresenter(LoginView view, LoginInteractor loginInteractor,
                          PelangganInterface pelangganInterface, SessionManager sessionManager) {
        this.view = view;
        this.loginInteractor = loginInteractor;
        this.apiService = pelangganInterface;
        this.sessionManager = sessionManager;
    }


//    public void onLoginButtonClick() {
//        String email = view.getEmail();
//        if (loginInteractor.isValidEmail(email)) {
//            view.showErrorEmptyEmail(R.string.error_invalid_email);
//            return;
//        }
//        String password = view.getPassword();
//        if (password.isEmpty()) {
//            view.showErrorEmptyPassword(R.string.error_invalid_password);
//            return;
//        }
//
//        if (loginInteractor.validateLogin(email, password)) {
////            loginInteractor.createLoginSession();
//            view.startMainActivity();
//            return;
//        }
//        view.showToastFailedLogin();
//    }

    public void onCheckButtonClick() {
        String email = view.getEmail();
        if (!loginInteractor.isValidEmail(email)) {
            view.showErrorEmptyEmail(R.string.error_invalid_email);
            return;
        }
        String password = view.getPassword();
        if (password.isEmpty()) {
            view.showErrorEmptyPassword(R.string.error_invalid_password);
            return;
        }

        view.showProgress(true);
        apiService.checkLogin(email, password).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                view.showProgress(false);
                if (response.body() != null) {
                    if (response.body().isError()) {
                        view.setErrorMessage(response.body().getMessage());
                    } else {
//                        if (response.body().getJabatan().equals("admin")) {
//                            view.startManagementActivity();
//                        } else {
                        loginInteractor.createLoginSession(sessionManager,response.body());
                        view.startMainActivity();
//                        }
                    }
                } else {
                    view.setErrorMessage(R.string.error_process);
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                view.showProgress(false);
                view.setErrorMessage(R.string.error_network);
            }
        });

    }

}
