package adriyo.makancepat.ui.transaksi;

/**
 * Created by adriyo on 02/05/17.
 */

class AlamatUserPresenter {

    private AlamatUserView mView;

    public AlamatUserPresenter(AlamatUserView mView) {
        this.mView = mView;
    }

    public void onSaveAlamatClicked() {
        String nama = mView.getNama();
        if (nama.isEmpty() || nama.length() < 6) {
            mView.displayErrorWhenNamaIsEmpty();
            return;
        }

        String noTelp = mView.getNoTelp();
        if (noTelp.isEmpty() || noTelp.length() < 10) {
            mView.displayErrorWhenNoTelpIsEmpty();
            return;
        }

        String alamat = mView.getAlamat();
        if (alamat.isEmpty() || alamat.length() < 20) {
            mView.displayErrorWhenAlamatIsEmpty();
            return;
        }

        mView.sendDataAlamatToCheckoutActivity();

    }
}
