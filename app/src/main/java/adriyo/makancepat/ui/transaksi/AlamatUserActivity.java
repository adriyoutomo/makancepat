package adriyo.makancepat.ui.transaksi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;

import adriyo.makancepat.R;
import adriyo.makancepat.model.UserModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AlamatUserActivity extends AppCompatActivity implements AlamatUserView {

    @BindView(R.id.checkout_alamat_et)
    EditText alamatEt;

    @BindView(R.id.checkout_nama_user_et)
    EditText namaEt;

    @BindView(R.id.checkout_no_telp_et)
    EditText noTelpEt;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private AlamatUserPresenter alamatUserPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alamat_user);
        ButterKnife.bind(this);

        initToolbar();

        alamatUserPresenter = new AlamatUserPresenter(this);

    }

    private void initToolbar() {
        if (getSupportActionBar() == null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(R.string.title_activity_isi_data_pemesan);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            });
        }
    }

    @OnClick(R.id.checkout_save_alamat_btn)
    public void onSaveAlamatClicked() {
        alamatUserPresenter.onSaveAlamatClicked();
    }

    @Override
    public String getNama() {
        return namaEt.getText().toString().trim();
    }

    @Override
    public String getNoTelp() {
        return noTelpEt.getText().toString().trim();
    }

    @Override
    public String getAlamat() {
        return alamatEt.getText().toString().trim();
    }

    @Override
    public void displayErrorWhenAlamatIsEmpty() {
        alamatEt.setError(getString(R.string.error_empty_alamat));
    }

    @Override
    public void displayErrorWhenNoTelpIsEmpty() {
        noTelpEt.setError(getString(R.string.error_empty_no_telp));
    }

    @Override
    public void displayErrorWhenNamaIsEmpty() {
        namaEt.setError(getString(R.string.error_empty_nama));
    }

    @Override
    public void sendDataAlamatToCheckoutActivity() {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        UserModel userModel = new UserModel();
        userModel.setAlamat(getAlamat());
        userModel.setFirstName(getNama());
        userModel.setPhone(getNoTelp());
        bundle.putSerializable("user_model", userModel);
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
}
