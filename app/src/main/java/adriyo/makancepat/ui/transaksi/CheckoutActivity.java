package adriyo.makancepat.ui.transaksi;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.adapter.CheckoutRVAdapter;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.DaftarPesananModel;
import adriyo.makancepat.model.DaftarPesananResponse;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.DaftarPesananInterface;
import adriyo.makancepat.view.DetailCheckoutActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity extends AppCompatActivity implements  CheckoutRVAdapter.CheckoutActivityListener {


    @BindView(R.id.layoutCheckout)
    LinearLayout layoutCheckout;
    @BindView(R.id.labelCheckoutTotalHarga)
    TextView labelTotalHarga;

    @BindView(R.id.rvCheckoutItem)
    RecyclerView checkoutItemRv;
    List<DaftarPesananModel> listMenu;
    SessionManager sessionManager;
    int grandTotal = 0;


    CheckoutRVAdapter adapter;
    int idUser;
    PlacePicker.IntentBuilder builder;
    ProgressDialog progressdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (getSupportActionBar() == null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(R.string.title_activity_checkout);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //NavUtils.navigateUpFromSameTask(CheckoutActivity.this);
                    finish();
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

                }
            });
        }
        init();

        idUser = Integer.parseInt(sessionManager.getUserDetails().get(SessionManager.KEY_ID_USER));

        refreshRV();

    }

    private void init() {
        builder = new PlacePicker.IntentBuilder();

        adapter = new CheckoutRVAdapter();

        sessionManager = new SessionManager(getApplicationContext());

        checkoutItemRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        progressdialog = new ProgressDialog(CheckoutActivity.this);
        progressdialog.setMessage("Please Wait....");
        progressdialog.setCanceledOnTouchOutside(false);
        progressdialog.setCancelable(false);

        showError(true);
    }

    @Override
    public void setTotalHarga(List<DaftarPesananModel> data) {
        int result = 0;
        for (DaftarPesananModel item : data) {
            result += (item.getHarga() * item.getQty());
        }
        labelTotalHarga.setText(Html.fromHtml(MyUtilities.setRupiah(String.valueOf(result))));
        adapter.notifyDataSetChanged();
        Log.d("totalharga", "is called");
    }

    @Override
    public void showError(boolean show) {
        if (show) layoutCheckout.setVisibility(View.GONE);
        else layoutCheckout.setVisibility(View.VISIBLE);
        Log.d("showerror", "is called");
    }

    @OnClick(R.id.btnCheckoutSetuju)
    public void onCheckoutSetujuClicked(){
        Intent intent = new Intent(this, DetailCheckoutActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @Override
    public void refreshRV() {
        requestData();
    }

    @Override
    public void showDialog(boolean show) {
        if (show) progressdialog.show();
        else progressdialog.dismiss();
    }

    public void requestData() {
        showDialog(true);
        DaftarPesananInterface apiService = Communicator.getClient().create(DaftarPesananInterface.class);
        Call<DaftarPesananResponse> call = apiService.getMenuMakananDetail(idUser);
        call.enqueue(new Callback<DaftarPesananResponse>() {
            @Override
            public void onResponse(Call<DaftarPesananResponse> call, Response<DaftarPesananResponse> response) {
                showDialog(false);
                if (response.body().isError()) {
                    Toast.makeText(CheckoutActivity.this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                }
                listMenu = response.body().getListMenu();
                adapter = new CheckoutRVAdapter(listMenu, CheckoutActivity.this, labelTotalHarga, layoutCheckout, CheckoutActivity.this);
                checkoutItemRv.setAdapter(adapter);
                setTotalHarga(listMenu);
                int size = listMenu.size();
                Log.d("sizeactive", size + "");
                if (size > 0) showError(false);
                else showError(true);
            }

            @Override
            public void onFailure(Call<DaftarPesananResponse> call, Throwable t) {
                // Log error here since request failed
                Toast.makeText(getApplicationContext(), "Failed to request data", Toast.LENGTH_SHORT).show();
                showDialog(false);
            }
        });
        adapter.notifyDataSetChanged();
    }

    public void showInfoDialog(String message) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        dialogBuilder.setTitle("Informasi");
        dialogBuilder.setMessage(message);
        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.setCancelable(true);
        b.setCanceledOnTouchOutside(false);
        b.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshRV();
//        Toast.makeText(this, "ON RESUME BITCH", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

    }
}
