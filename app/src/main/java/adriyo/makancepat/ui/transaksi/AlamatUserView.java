package adriyo.makancepat.ui.transaksi;

/**
 * Created by adriyo on 02/05/17.
 */

interface AlamatUserView {
    String getNama();

    String getNoTelp();

    String getAlamat();

    void displayErrorWhenAlamatIsEmpty();

    void displayErrorWhenNoTelpIsEmpty();

    void displayErrorWhenNamaIsEmpty();

    void sendDataAlamatToCheckoutActivity();
}
