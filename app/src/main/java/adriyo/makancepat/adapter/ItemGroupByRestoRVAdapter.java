package adriyo.makancepat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.MyStatic;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.model.DistanceModel;
import adriyo.makancepat.model.DurationModel;
import adriyo.makancepat.model.ElementsModel;
import adriyo.makancepat.model.MapInformationModel;
import adriyo.makancepat.model.MenuMakananModel;
import adriyo.makancepat.model.RestoModel;
import adriyo.makancepat.model.RowsModel;
import adriyo.makancepat.rest.MyMapService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.R.attr.mode;

/**
 * Created by ADMIN on 1/20/2017.
 */

public class ItemGroupByRestoRVAdapter extends RecyclerView.Adapter<ItemGroupByRestoRVAdapter.ViewHolder> {

    private List<MenuMakananModel> list;
    private Context context;
    private MyUtilities myUtilities;

    public ItemGroupByRestoRVAdapter(List<MenuMakananModel> list, Context context) {
        this.list = list;
        this.context = context;
        myUtilities = new MyUtilities();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_grup_detail_checkout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MenuMakananModel model = list.get(position);
        holder.txtNamaMenu.setText(model.getNamaMenu());
        holder.txtTotal.setText(MyUtilities.setRupiah(model.getHarga())+" x "+model.getQty());
        int total = Integer.parseInt(model.getHarga()) * model.getQty();
        holder.txtTotalHarga.setText(MyUtilities.setRupiah(total+""));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView txtNamaMenu, txtTotal, txtTotalHarga;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            txtNamaMenu = (TextView) view.findViewById(R.id.labelItemGrupMenu);
            txtTotal = (TextView) view.findViewById(R.id.labelItemGrupTotal);
            txtTotalHarga = (TextView) view.findViewById(R.id.labelItemGrupTotalHarga);
        }
    }

}
