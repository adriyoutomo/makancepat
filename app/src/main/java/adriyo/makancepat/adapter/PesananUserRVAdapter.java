package adriyo.makancepat.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.model.DaftarPesananUserModel;

public class PesananUserRVAdapter extends RecyclerView.Adapter<PesananUserRVAdapter.ViewHolder> {

    private final List<DaftarPesananUserModel> mValues;
    private String formatDate = "dd MMMM yyyy";

    public PesananUserRVAdapter(List<DaftarPesananUserModel> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item_daftar_pesanan_user, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        String id = mValues.get(position).getIdTransaksi()+"";
        String rowId = MyUtilities.getTanggal(mValues.get(position).getWaktuTransaksi(), "ddMMyyyy") +"/"+id;
        holder.idTransaksi.setText(rowId);
        String tanggal = MyUtilities.getTanggal(mValues.get(position).getWaktuTransaksi(), "dd MMMM yyyy");
        holder.tanggalTransaksi.setText(tanggal);

        String nominal = MyUtilities.setRupiah(holder.mItem.getTotalTransaksi()+"");
        holder.totalHarga.setText(Html.fromHtml("<b>"+nominal+"</b>"));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView idTransaksi;
        public final TextView tanggalTransaksi;
        public final TextView totalHarga;
        public DaftarPesananUserModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            idTransaksi = (TextView) view.findViewById(R.id.idTransaksiDaftarPesanan);
            tanggalTransaksi = (TextView) view.findViewById(R.id.tanggalDaftarPesanan);
            totalHarga = (TextView) view.findViewById(R.id.totalHargaDaftarPesanan);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tanggalTransaksi.getText() + "'";
        }
    }
}
