package adriyo.makancepat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.SessionManager;
import adriyo.makancepat.model.DaftarPesananModel;
import adriyo.makancepat.model.DaftarPesananResponse;
import adriyo.makancepat.model.Movie;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.DaftarPesananInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarPesananRestoRVAdapter extends RecyclerView.Adapter<DaftarPesananRestoRVAdapter.ViewHolder> {

    private List<DaftarPesananModel> mValues;
    private Context context;
    private SessionManager sessionManager;
    TextView labelHarga;
    CheckoutActivityListener listener;
    LinearLayout layoutCheckout;

    public DaftarPesananRestoRVAdapter(List<DaftarPesananModel> mValues, Context context,
                                       TextView labelHarga, LinearLayout layoutCheckout, CheckoutActivityListener listener) {
        this.mValues = mValues;
        this.context = context;
        sessionManager = new SessionManager(context);
        this.labelHarga = labelHarga;
        this.layoutCheckout = layoutCheckout;
        this.listener = listener;
    }

    public DaftarPesananRestoRVAdapter() {
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.checkout_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.labelNamaMenu.setText(mValues.get(position).getNamaMenu());
        holder.labelHarga.setText("Rp. " + mValues.get(position).getHarga() + " x " + mValues.get(position).getQty());
        holder.btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DaftarPesananInterface apiService = Communicator.getClient().create(DaftarPesananInterface.class);
                Call<DaftarPesananResponse> call = apiService.deleteItemDetailTransaksi(holder.mItem.getId());
                call.enqueue(new Callback<DaftarPesananResponse>() {
                    @Override
                    public void onResponse(Call<DaftarPesananResponse> call, Response<DaftarPesananResponse> response) {

                    }

                    @Override
                    public void onFailure(Call<DaftarPesananResponse> call, Throwable t) {

                    }
                });

                int idUser = Integer.parseInt(sessionManager.getUserDetails().get(SessionManager.KEY_ID_USER));
                call = apiService.getMenuMakananDetail(idUser);
                call.enqueue(new Callback<DaftarPesananResponse>() {
                    @Override
                    public void onResponse(Call<DaftarPesananResponse> call, Response<DaftarPesananResponse> response) {
                        swap(response.body().getListMenu());
                        listener.refreshRV();
                        /*Log.d("size", mValues.size() + "");
                        listener.setTotalHarga(mValues);
                        int size = mValues.size();
                        if (size > 0)
                            listener.showError(false);
                        else
                            listener.showError(true);*/
                    }

                    @Override
                    public void onFailure(Call<DaftarPesananResponse> call, Throwable t) {

                    }
                });

            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView labelNamaMenu;
        public final TextView labelHarga;
        public final ImageButton btnHapus;
        public DaftarPesananModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            labelNamaMenu = (TextView) view.findViewById(R.id.labelCheckoutNamaMenu);
            labelHarga = (TextView) view.findViewById(R.id.labelCheckoutHarga);
            btnHapus = (ImageButton) view.findViewById(R.id.btnCheckoutHapusItem);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + labelHarga.getText() + "'";
        }
    }

    public void swap(List<DaftarPesananModel> datas) {
        mValues.clear();
        mValues.addAll(datas);
        notifyDataSetChanged();
    }

    public interface CheckoutActivityListener {
        void setTotalHarga(List<DaftarPesananModel> data);
        void showError(boolean show);
        void refreshRV();
    }

    public void setTotalHarga(List<DaftarPesananModel> models) {
        int result = 0;
        for (DaftarPesananModel data : models) {
            result += (data.getHarga() * data.getQty());
        }
        labelHarga.setText(Html.fromHtml("<h2>Total Pembayaran: Rp." + result + "</h2>"));
    }

    public void showError(boolean show) {
        if (show) layoutCheckout.setVisibility(View.GONE);
        else layoutCheckout.setVisibility(View.VISIBLE);
        Log.d("showerror", "is called");
    }
}
