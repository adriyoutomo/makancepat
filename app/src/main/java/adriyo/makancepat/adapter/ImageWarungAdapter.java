package adriyo.makancepat.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.model.ImageWarungModel;

/**
 * Created by ADMIN on 8/3/2016.
 */

public class ImageWarungAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater mLayoutInflater;
    private List<ImageWarungModel> data;

    public ImageWarungAdapter(Context context, List<ImageWarungModel> data) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        String url = data.get(position).getUrl();
        View itemView = mLayoutInflater.inflate(R.layout.warung_pager_item, container, false);
        ImageView mImageView = (ImageView) itemView.findViewById(R.id.imageView);
        mImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        Log.d("imgwarung",data.get(position).getUrl());
        Picasso.with(context).load(data.get(position).getUrl()).into(mImageView);
        //mImageView.setImageResource(sliderImagesId[position]);
        //((ViewPager) container).addView(mImageView, 0);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //((ViewPager) container).removeView((ImageView) object);
        container.removeView((LinearLayout) object);
    }


}
