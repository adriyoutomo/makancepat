package adriyo.makancepat.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.model.DetailPesananUserModel;
import de.hdodenhof.circleimageview.CircleImageView;

public class DetailPesananUserRVAdapter extends RecyclerView.Adapter<DetailPesananUserRVAdapter.ViewHolder> {

    private final List<DetailPesananUserModel> mValues;
    private String formatDate = "dd MMMM yyyy";

    public DetailPesananUserRVAdapter(List<DetailPesananUserModel> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail_pemesanan_user, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.menuMakanan.setText(mValues.get(position).getNamaMenu() + "");
        holder.warungMakan.setText(holder.mItem.getNamaWarung());

        String nominal = MyUtilities.setRupiah(holder.mItem.getHarga() + "");
        int qty = holder.mItem.getQty();
        String row = nominal + " x "+ qty + " item";
        holder.hargaMakanan.setText(Html.fromHtml("<b>" + row + "</b>"));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView menuMakanan;
        public final TextView hargaMakanan;
        public final TextView warungMakan;
        public final CircleImageView imgMakanan;
        public DetailPesananUserModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            menuMakanan = (TextView) view.findViewById(R.id.txtMenuDetailPemesananUser);
            hargaMakanan = (TextView) view.findViewById(R.id.labelHargaDetailPemesananUser);
            warungMakan = (TextView) view.findViewById(R.id.labelWarungDetailPemesananUser);
            imgMakanan = (CircleImageView) view.findViewById(R.id.imgMenuDetailPesananUser);
        }
    }
}
