package adriyo.makancepat.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.model.MenuRestoModel;
import adriyo.makancepat.view.DetailMakananActivity;

/**
 * Created by admin on 11/30/2016.
 */

public class MenuRestoAdapter extends RecyclerView.Adapter<MenuRestoAdapter.ViewHolder> {


    private final List<MenuRestoModel> mValues;
    private Context context;

    public MenuRestoAdapter(List<MenuRestoModel> items, Context context) {
        mValues = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_makanan_resto_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mNamaMakanan.setText(mValues.get(position).getNamaMenu());
        holder.mHargaMakanan.setText("Rp. "+mValues.get(position).getHarga());
        Log.d("url_image",mValues.get(position).getUrl());
        Picasso.with(context).load(mValues.get(position).getUrl()).into(holder.mImageMenuMakanan);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public final View mView;
        public final Button btnPilih;
        public final ImageView mImageMenuMakanan;
        public final TextView mNamaMakanan;
        public final TextView mHargaMakanan;
        public final CardView cardView;
        public MenuRestoModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            cardView = (CardView) view.findViewById(R.id.cardViewMenu);
            btnPilih = (Button) view.findViewById(R.id.btnPilih);
            mImageMenuMakanan = (ImageView) view.findViewById(R.id.imgMenuMakanan);
            mNamaMakanan = (TextView) view.findViewById(R.id.labelNamaMakanan);
            mHargaMakanan = (TextView)view.findViewById(R.id.labelHargaMakanan);
            btnPilih.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btnPilih :

                    Intent intent = new Intent(view.getContext(), DetailMakananActivity.class);
                    intent.putExtra("idMakanan", mItem.getIdMenu()+"");
                    view.getContext().startActivity(intent);
                    //Toast.makeText(view.getContext(), mItem.getIdMenu()+"", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;

            }
        }

    }
}
