package adriyo.makancepat.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import adriyo.makancepat.R;
import adriyo.makancepat.rest.Communicator;

/**
 * Created by ADMIN on 8/3/2016.
 */

public class BannerSliderAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater mLayoutInflater;
    private int totalPage = 0;
    private int[] sliderImagesId = new int[]{
            R.drawable.drawer_background_material
    };

    private String[] urlImages = new String[]{
            Communicator.BASE_URL+"v1/image_data/banner/banner1.jpg",
            "http://makancepat.com/foto_banner/DAFTARKAN%20OUTLETMU.JPG",
            "http://makancepat.com/foto_banner/IMG_20160115_193002.jpg"
    };

    public BannerSliderAdapter(Context context) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return urlImages.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView mImageView = new ImageView(this.context);
        mImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        container.addView(mImageView, 0);
        Picasso.with(this.context)
                .load(urlImages[position])
                .placeholder(sliderImagesId[0])
                .into(mImageView);
        return mImageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }
}
