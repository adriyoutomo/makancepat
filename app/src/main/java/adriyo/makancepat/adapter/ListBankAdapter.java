package adriyo.makancepat.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import adriyo.makancepat.R;

/**
 * Created by adriyo on 3/17/2017.
 */

public class ListBankAdapter extends BaseExpandableListAdapter {


    private Context context;
    private List<String> listHeader;
    private HashMap<String, List<String>> listChild;

    public ListBankAdapter(Context context, List<String> listHeader, HashMap<String, List<String>> listChild) {
        this.context = context;
        this.listHeader = listHeader;
        this.listChild = listChild;
    }

    @Override
    public int getGroupCount() {
        return this.listHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int childrenCount = 1;
        childrenCount = this.listChild.get(this.listHeader.get(groupPosition)).size();
        return childrenCount;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.listChild.get(this.listHeader.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        List<String> data = listChild.get(listHeader.get(groupPosition));
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_bank_topup, null);
        }
        ImageView imgLogo = (ImageView) convertView.findViewById(R.id.img_logo_bank);
        ImageView imgArrow = (ImageView) convertView.findViewById(R.id.img_arrow);

        switch (groupPosition) {
            case 0:
                imgLogo.setImageResource(R.drawable.logo_bca_1);
                break;
            case 1:
                imgLogo.setImageResource(R.drawable.logo_mandiri_1);
                break;
            default:
                imgLogo.setImageResource(R.drawable.logo_bca_1);
                break;
        }

        if (data.size() > 0) {
            if (!isExpanded) {
                imgArrow.setImageResource(R.drawable.ic_action_expand);
            } else {
                imgArrow.setImageResource(R.drawable.ic_action_collapse);
            }
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_bank_no_rek, null);
        }
        Log.d("childView", childText);
        TextView txtListChild = (TextView) convertView.findViewById(R.id.txtBankNoRek);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
