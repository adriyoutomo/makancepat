package adriyo.makancepat.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.model.GaleriMakananResponse;
import adriyo.makancepat.model.ImageMakananModel;
import adriyo.makancepat.rest.Communicator;
import adriyo.makancepat.rest.GaleriMakananInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ADMIN on 8/3/2016.
 */

public class ImageMakananAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater mLayoutInflater;
    private int totalPage = 0;
    private int[] sliderImagesId = new int[]{
            R.drawable.drawer_background_material, R.drawable.drawer_background_material, R.drawable.drawer_background_material,
            R.drawable.drawer_background_material, R.drawable.drawer_background_material, R.drawable.drawer_background_material
    };

    private List<ImageMakananModel> data;

    public ImageMakananAdapter(Context context, List<ImageMakananModel> data) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        String url = data.get(position).getUrl();
        View itemView = mLayoutInflater.inflate(R.layout.fragment_image_makanan, container, false);
        ImageView mImageView = new ImageView(this.context);
        mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Picasso.with(context).load(url).into(mImageView);
        //mImageView.setImageResource(sliderImagesId[position]);
        ((ViewPager) container).addView(mImageView, 0);
        return mImageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }


}
