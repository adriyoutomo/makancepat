package adriyo.makancepat.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.model.RestoModel;
import adriyo.makancepat.view.MainActivity;

/**
 * Created by ADMIN on 1/20/2017.
 */

public class GroupByRestoRVAdapter extends RecyclerView.Adapter<GroupByRestoRVAdapter.ViewHolder> {

    private List<RestoModel> list;
    private Context context;
    private MyUtilities myUtilities;
    private ItemGroupByRestoRVAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public GroupByRestoRVAdapter(List<RestoModel> list, Context context) {
        this.list = list;
        this.context = context;
        myUtilities = new MyUtilities();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grup_detail_checkout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RestoModel model = list.get(position);
        holder.txtNamaResto.setText(model.getNamaWarung());
        adapter = new ItemGroupByRestoRVAdapter(model.getListMakanan(), context);
        mLayoutManager = new LinearLayoutManager(context);
        holder.rvItemPesanan.setLayoutManager(mLayoutManager);
        holder.rvItemPesanan.setAdapter(adapter);
        holder.rvItemPesanan.setHasFixedSize(true);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView txtNamaResto;
        public final RecyclerView rvItemPesanan;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            txtNamaResto = (TextView) view.findViewById(R.id.txtGrupNamaRestoCheckout);
            rvItemPesanan = (RecyclerView) view.findViewById(R.id.rvItemPesananResto);
        }
    }

    public interface GroupByRestoActivityListener {

        void showBtnLanjut(boolean show);
        void showDialog(boolean show, String message);
    }

}
