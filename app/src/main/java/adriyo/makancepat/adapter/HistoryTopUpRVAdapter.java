package adriyo.makancepat.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.model.Model_TopUp;

/**
 * Created by ADMIN on 2/15/2017.
 */

public class HistoryTopUpRVAdapter extends RecyclerView.Adapter<HistoryTopUpRVAdapter.ViewHolder> {

    private List<Model_TopUp> list;

    public HistoryTopUpRVAdapter() {
    }

    public HistoryTopUpRVAdapter(List<Model_TopUp> list) {
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail_history_top_up, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String id = list.get(position).getId() + "";
        String rowId = MyUtilities.getTanggal(list.get(position).getCreated_at(), "ddMMyyyy") + "/" + id;
        holder.idTopUp.setText(rowId);

        String tanggal = MyUtilities.getTanggal(list.get(position).getCreated_at(), "dd MMMM yyyy");
        holder.tanggalTopUp.setText(tanggal);

        String nominal = MyUtilities.setRupiah(list.get(position).getSaldo_awal() + "");
        holder.besaranTopUp.setText(Html.fromHtml("<b>" + nominal + "</b>"));

        String status = "";
        if (list.get(position).getIs_paid() == 0) status = "Belum divalidasi";
        else if (list.get(position).getIs_paid() == -1) status = "Ditolak";
        else if (list.get(position).getIs_paid() == 1) status = "Sudah divalidasi";
        else status = "Belum divalidasi";
        holder.statusTopUp.setText(status);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        public final TextView idTopUp, tanggalTopUp, besaranTopUp, statusTopUp;

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;

            idTopUp = (TextView) view.findViewById(R.id.labelIdPemasukan);
            tanggalTopUp = (TextView) view.findViewById(R.id.tanggalTopUp);
            besaranTopUp = (TextView) view.findViewById(R.id.labelBesaranTopUp);
            statusTopUp = (TextView) view.findViewById(R.id.labelStatusValidasi);
        }
    }
}
