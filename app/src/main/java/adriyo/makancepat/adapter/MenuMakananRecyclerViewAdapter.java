package adriyo.makancepat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.model.MenuMakananModel;
import adriyo.makancepat.view.MenuMakananFragment;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MenuMakananModel} and makes a call to the
 * specified {@link adriyo.makancepat.view.MenuMakananFragment.OnFragmentInteractionListener}.
 *
 */
public class MenuMakananRecyclerViewAdapter extends RecyclerView.Adapter<MenuMakananRecyclerViewAdapter.ViewHolder> {

    private final List<MenuMakananModel> mValues;
    private final MenuMakananFragment.OnFragmentInteractionListener mListener;
    private Context context;

    public MenuMakananRecyclerViewAdapter(List<MenuMakananModel> items, MenuMakananFragment.OnFragmentInteractionListener listener, Context context) {
        mValues = items;
        mListener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_menu_makanan_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mNamaMakanan.setText(mValues.get(position).getNamaMenu());
        //holder.mImageMenuMakanan.setImageResource(R.drawable.login_image_1);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onFragmentInteraction(holder.mItem);
                }
            }
        });

        Picasso.with(context).load(mValues.get(position).getUrl()).into(holder.mImageMenuMakanan);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageMenuMakanan;
        public final TextView mNamaMakanan;
        public MenuMakananModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageMenuMakanan = (ImageView) view.findViewById(R.id.imgMenuMakanan);
            mNamaMakanan = (TextView) view.findViewById(R.id.labelNamaMakanan);
        }
    }
}
