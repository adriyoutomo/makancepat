package adriyo.makancepat.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.model.MenuMakananModel;
import adriyo.makancepat.model.RestoModel;
import adriyo.makancepat.view.RestoFragment;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MenuMakananModel} and makes a call to the
 * specified {@link adriyo.makancepat.view.RestoFragment.OnListFragmentInteractionListener}.
 *
 */
public class RestoRecyclerViewAdapter extends RecyclerView.Adapter<RestoRecyclerViewAdapter.ViewHolder> {

    private final List<RestoModel> mValues;
    private final RestoFragment.OnListFragmentInteractionListener mListener;

    public RestoRecyclerViewAdapter(List<RestoModel> items, RestoFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_resto_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.imgResto.setImageResource(R.drawable.drawer_background_material);
        holder.labelNamaResto.setText(mValues.get(position).getNamaWarung());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView imgResto;
        public final TextView labelNamaResto;
        public RestoModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            imgResto = (ImageView) view.findViewById(R.id.imgResto);
            labelNamaResto = (TextView) view.findViewById(R.id.labelNamaResto);
        }
    }
}
