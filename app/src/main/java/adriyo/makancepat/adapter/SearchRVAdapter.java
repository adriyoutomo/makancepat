package adriyo.makancepat.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import adriyo.makancepat.R;
import adriyo.makancepat.customutil.MyUtilities;
import adriyo.makancepat.model.MenuMakananModel;

/**
 * Created by ADMIN on 11/30/2016.
 */

public class SearchRVAdapter extends RecyclerView.Adapter<SearchRVAdapter.ViewHolder> {

    private List<MenuMakananModel> model;

    public SearchRVAdapter(List<MenuMakananModel> model) {
        this.model = model;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_result, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(model.get(position));
    }

    @Override
    public int getItemCount() {
        return model.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView searchMenu;
        public TextView searchHarga;

        public ViewHolder(View itemView) {
            super(itemView);
            searchMenu = (TextView) itemView.findViewById(R.id.labelSearchMenuMakanan);
            searchHarga = (TextView) itemView.findViewById(R.id.labelSearchHarga);
        }

        /**
         * Set data to itemviews
         * @param data
         */
        public void bind(MenuMakananModel data){
            searchMenu.setText(data.getNamaMenu());
            String nominal = MyUtilities.setRupiah(data.getHarga());
            searchHarga.setText(nominal);
        }
    }
}
