package adriyo.makancepat.slider;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adriyo on 3/16/2017.
 */

public class SliderPagerAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = "SliderPagerAdapter";
    private Context context;
    List<Fragment> mFrags = new ArrayList<>();

    public SliderPagerAdapter(FragmentManager fm, List<Fragment> frags) {
        super(fm);
        mFrags = frags;

    }

    @Override
    public Fragment getItem(int position) {
        int index = position % mFrags.size();
        return FragmentSlider.newInstance(mFrags.get(index).getArguments().getString("params"));

    }



    @Override
    public int getCount() {
//        return urlImages.length;
        return Integer.MAX_VALUE;
    }

}
