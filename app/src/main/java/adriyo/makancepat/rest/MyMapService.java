package adriyo.makancepat.rest;

import adriyo.makancepat.model.MapInformationModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by ADMIN on 1/15/2017.
 */

public interface MyMapService {

    @GET("/maps/api/distancematrix/json?units=imperial")
    Call<MapInformationModel> getMapInformation(@Query("origins") String origins,
                                                @Query("destinations") String destinations,
                                                @Query("key") String apiKey);
}
