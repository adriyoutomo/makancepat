package adriyo.makancepat.rest;

import adriyo.makancepat.model.MenuMakananModel;
import adriyo.makancepat.model.MenuMakananResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ADMIN on 11/1/2016.
 */

public interface MenuMakananInterface {

    @FormUrlEncoded
    @POST("v1/menumakanan")
    Call<MenuMakananResponse> getMenuMakanan(@Field("authorization") String authKey);

    @FormUrlEncoded
    @POST("v1/menumakanandetail")
    Call<MenuMakananModel> getMenuMakananDetail(@Field("id") int id);
}
