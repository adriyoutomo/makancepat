package adriyo.makancepat.rest;

import adriyo.makancepat.model.MenuRestoResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by admin on 12/1/2016.
 */

public interface MenuRestoInterface {

    @FormUrlEncoded
    @POST("v1/makanan_warung")
    Call<MenuRestoResponse> getMakananResto(@Field("id_warung") int idWarung);

    @FormUrlEncoded
    @POST("v1/minuman_warung")
    Call<MenuRestoResponse> getMinumanResto(@Field("id_warung") int idWarung);
}
