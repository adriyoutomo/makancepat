package adriyo.makancepat.rest;

import adriyo.makancepat.model.DaftarPesananResponse;
import adriyo.makancepat.model.DaftarPesananRestoResponse;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by ADMIN on 11/14/2016.
 */

public interface DaftarPesananInterface {
    @FormUrlEncoded
    @POST("v1/detail_transaksi")
    Call<DaftarPesananResponse> getMenuMakananDetail(@Field("id_user") int idUser);

    @DELETE("v1/detail_transaksi/{id}")
    Call<DaftarPesananResponse> deleteItemDetailTransaksi(@Path("id") int id);

    @FormUrlEncoded
    @POST("v1/transaksi/{id_user}")
    Call<DaftarPesananResponse> insertTransaksi(@Path("id_user") int id_user, @Field("id_menu") int id_menu
            , @Field("qty") int qty
            , @Field("note") String note);

    @GET("v1/resto_daftar_pesanan")
    Call<DaftarPesananRestoResponse> getDaftarPesananResto();
}
