package adriyo.makancepat.rest;

import adriyo.makancepat.model.InfoModel;
import adriyo.makancepat.model.Model_TopUp_Response;
import adriyo.makancepat.model.SaldoModel;
import adriyo.makancepat.model.UserModel;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by ADMIN on 9/4/2016.
 */

public interface PelangganInterface {
    /**
     * @param email
     * @param password
     * @return
     */
    @FormUrlEncoded
    @POST("v1/login")
    Call<UserModel> checkLogin(@Field("email") String email, @Field("password") String password);


    @FormUrlEncoded
    @POST("v1/register")
    Call<UserModel> createUser(@Field("username") String username,
                               @Field("first_name") String firstName,
                               @Field("last_name") String lastName,
                               @Field("phone") String noTelp,
                               @Field("alamat") String alamat,
                               @Field("bank") String bank,
                               @Field("no_rekening") String noRekening,
                               @Field("no_ktp") String noKTP,
                               @Field("referal") String referal,
                               @Field("email") String email,
                               @Field("password") String password);

    @FormUrlEncoded
    @POST("v1/user")
    Call<UserModel> getUser(@Field("email") String email);

    @FormUrlEncoded
    @POST("v1/send_verification_email")
    Call<InfoModel> sendVerificationEmail(@Field("email") String email);

    @FormUrlEncoded
    @PUT("v1/update_pass")
    Call<UserModel> updatePassword(
            @Field("old_pass") String oldPass,
            @Field("new_pass") String newPassword,
            @Field("email") String email
    );

    @FormUrlEncoded
    @PUT("v1/user/{id_user}")
    Call<UserModel> updateProfilUser(@Path("id_user") String idUser,
                                     @Field("first_name") String firstName,
                                     @Field("last_name") String lastName,
                                     @Field("phone") String noTelp,
                                     @Field("no_ktp") String noKtp,
                                     @Field("no_rekening") String noRekening,
                                     @Field("alamat") String alamat,
                                     @Field("bank") String bank);

    @Multipart
    @POST("v1/upload_profile_picture")
    Call<UserModel> uploadProfilePicture(
            @Part MultipartBody.Part file,
            @Part("id_user") RequestBody idUser,
            @Part("last_foto") RequestBody lastFoto
    );

    @FormUrlEncoded
    @POST("v1/saldo_pelanggan")
    Call<SaldoModel> saldoPelanggan(@Field("id_user") int idUser);

    @FormUrlEncoded
    @POST("user/username_is_valid")
    Call<UserModel> usernameIsValid(@Field("username") String username);

    @FormUrlEncoded
    @POST("user/reset_password")
    Call<InfoModel> resetPassword(@Field("email") String email, @Field("phone") String phone);


    @FormUrlEncoded
    @POST("user/history_top_up")
    Call<Model_TopUp_Response> geHistoryTopUp(@Field("id_user") String idUser);

    /**
     * Test with observable
     *
     * @param idUser
     * @return
     */
    @FormUrlEncoded
    @POST("v1/saldo_pelanggan")
    Observable<SaldoModel> getSaldo(@Field("id_user") int idUser);

    @FormUrlEncoded
    @POST("user/update_password")
    Call<UserModel> updatePasswordUser(
            @Field("email") String email,
            @Field("old_password") String oldPassword,
            @Field("new_password") String newPassword
    );

}
