package adriyo.makancepat.rest;

import adriyo.makancepat.model.GaleriMakananResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ADMIN on 11/20/2016.
 */

public interface GaleriMakananInterface {

    @FormUrlEncoded
    @POST("v1/galeri_menu_makanan")
    Call<GaleriMakananResponse> getGaleriMakanan(@Field("id_menu") int idMenu);

}
