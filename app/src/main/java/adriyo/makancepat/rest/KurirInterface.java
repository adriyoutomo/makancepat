package adriyo.makancepat.rest;

import adriyo.makancepat.model.KurirModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ADMIN on 1/21/2017.
 */

public interface KurirInterface {

    @FormUrlEncoded
    @POST("v1/kurir_transaksi")
    Call<KurirModel> getKurirTransaksi(@Field("authorization") String authKey);

    @FormUrlEncoded
    @POST("v1/add_rating_kurir")
    Call<KurirModel> addRatingKurir(@Field("id_transaksi") int idTransaksi
            , @Field("rate") int rate
            , @Field("id_kurir") int idKurir
            , @Field("note") String note);
}
