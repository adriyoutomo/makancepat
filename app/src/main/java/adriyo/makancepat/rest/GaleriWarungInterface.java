package adriyo.makancepat.rest;

import adriyo.makancepat.model.GaleriWarungResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by admin on 12/2/2016.
 */

public interface GaleriWarungInterface {
    @FormUrlEncoded
    @POST("v1/galeri_warung")
    Call<GaleriWarungResponse> getGaleriWarung(@Field("id_warung") int idWarung);
}
