package adriyo.makancepat.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by adriyo on 02/05/17.
 */

class DefaultResponse {

    @SerializedName("error")
    private boolean error;

    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
