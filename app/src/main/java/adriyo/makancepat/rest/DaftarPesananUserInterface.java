package adriyo.makancepat.rest;

import adriyo.makancepat.model.DaftarPesananUserResponse;
import adriyo.makancepat.model.DetailPesananUserResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ADMIN on 11/29/2016.
 */

public interface DaftarPesananUserInterface {

    @FormUrlEncoded
    @POST("v1/user_history_pemesanan")
    Call<DaftarPesananUserResponse> getDaftarPesanan(@Field("id_user") int idUser);

    @FormUrlEncoded
    @POST("v1/user_detail_history_pemesanan")
    Call<DetailPesananUserResponse> getDetailDaftarPesanan(@Field("id_transaksi") int idUser);
}
