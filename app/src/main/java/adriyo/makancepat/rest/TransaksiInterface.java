package adriyo.makancepat.rest;

import adriyo.makancepat.model.InfoModel;
import adriyo.makancepat.model.KurirModel;
import adriyo.makancepat.model.ListCheckoutModel;
import adriyo.makancepat.model.TransaksiModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by ADMIN on 11/15/2016.
 */

public interface TransaksiInterface {
    @FormUrlEncoded
    @PUT("v1/transaksi/{id_transaksi}")
    Call<TransaksiModel> updateTransaksi(
            @Path("id_transaksi") int idTransaksi
            , @Field("id_kurir") int idKurir
            , @Field("total_transaksi") int totalTransaksi
            , @Field("lattitude") String jumlahDibayar
            , @Field("longitude") String kembalian);


    @FormUrlEncoded
    @POST("v1/list_checkout")
    Call<ListCheckoutModel> getDetailCheckout(@Field("id_user") int idUser);

    @FormUrlEncoded
    @POST("v1/total_harga_transaksi")
    Call<TransaksiModel> getTotalHarga(@Field("id_user") double idUser);

    @FormUrlEncoded
    @POST("v1/data_transaksi")
    Call<TransaksiModel> getDataTransaksi(@Field("id_transaksi") int idTransaksi);

    @FormUrlEncoded
    @POST("v1/kurir_by_id_transaksi")
    Call<KurirModel> getKurirByIDTransaksi(@Field("id_transaksi") int idTransaksi);

    /**
     * transaksi cash
     *
     * @param idTransaksi
     * @param idKurir
     * @param totalTransaksi
     * @param lattitude
     * @param longitude
     * @param idUser
     * @param username
     * @param totalHarga
     * @param idPemasukan
     * @return
     */
    @FormUrlEncoded
    @POST("v1/newtransaksi/{id_transaksi}")
    Call<TransaksiModel> checkoutTransaksi(
            @Path("id_transaksi") int idTransaksi,
            @Field("id_kurir") int idKurir,
            @Field("total_transaksi") int totalTransaksi,
            @Field("lattitude") String lattitude,
            @Field("longitude") String longitude,
            @Field("id_user") String idUser,
            @Field("username") String username,
            @Field("besaran") int totalHarga,
            @Field("id_pemasukan") int idPemasukan,
            @Field("nama_pemesan") String namaPemesan,
            @Field("no_telp_pemesan") String noTelpPemesan,
            @Field("alamat_pemesan") String alamatPemesan
            // TODO menambahkan data_pemesan ke transaksi

    );

    @FormUrlEncoded
    @POST("v1/transaksi_saldo/{id_transaksi}")
    Call<TransaksiModel> checkoutTransaksiSaldo(
            @Path("id_transaksi") int idTransaksi,
            @Field("id_kurir") int idKurir,
            @Field("total_transaksi") int totalTransaksi,
            @Field("lattitude") String lattitude,
            @Field("longitude") String longitude,
            @Field("id_user") String idUser,
            @Field("username") String username,
            @Field("besaran") int totalHarga,
            @Field("bayar_saldo") int bayarSaldo,
            @Field("id_pemasukan") int idPemasukan,
            @Field("nama_pemesan") String namaPemesan,
            @Field("no_telp_pemesan") String noTelpPemesan,
            @Field("alamat_pemesan") String alamatPemesan
            // TODO menambahkan data_pemesan ke transaksi
    );

    @FormUrlEncoded
    @POST("user/pencairan")
    Call<InfoModel> postPencairan(@Field("id_user") String idUser, @Field("besaran") String besaran);

    @FormUrlEncoded
    @POST("v1/transaksi_alamat")
    Call<DefaultResponse> saveAlamatTransaksi(
            @Field("nama_user") String namaUser,
            @Field("no_telp") String noTelp,
            @Field("alamat") String alamat

    );
}
