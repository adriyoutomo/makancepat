package adriyo.makancepat.rest;

import adriyo.makancepat.model.RestoModel;
import adriyo.makancepat.model.RestoResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ADMIN on 11/3/2016.
 */

public interface RestoInterface {

    @FormUrlEncoded
    @POST("v1/warungmakan")
    Call<RestoResponse> getResto(@Field("authorization") String authKey);

    @FormUrlEncoded
    @POST("v1/warungmakandetail")
    Call<RestoResponse> getRestoById(@Field("id") int id);

    @FormUrlEncoded
    @POST("v1/detail_warung")
    Call<RestoModel> getDetailRestoById(@Field("id_warung") int idWarung);
}
