package adriyo.makancepat.rest;

import java.util.List;

import adriyo.makancepat.model.MenuMakananModel;
import adriyo.makancepat.model.SearchItem;
import adriyo.makancepat.model.SearchResultResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ADMIN on 11/30/2016.
 */

public interface SearchService {
    @FormUrlEncoded
    @POST("v1/search")
    Call<SearchResultResponse> searchData(@Field("teks") String teks);
}
