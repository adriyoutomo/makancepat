package adriyo.makancepat.rest;

import adriyo.makancepat.model.Model_TopUp;
import adriyo.makancepat.model.Model_TopUp_Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by USER on 11/10/2016.
 */

public interface Interface_TopUp {
    @POST("v1/top_up_upload")
    @FormUrlEncoded
    Call<Model_TopUp> postImage(
            @Field("id_user") String idUser,
            @Field("saldo_awal") int saldo_awal,
            @Field("images") String images_encode64,
            @Field("jenis_transaksi") int jenisTransaksi
    );

    @GET("v1/top_up")
    Call<Model_TopUp_Response> getNewTopUp();

    @PUT("v1/top_up/{id_pemasukan}")
    Call<Model_TopUp> updateTopUp(@Path("id_pemasukan") int id_pemasukan);
}
